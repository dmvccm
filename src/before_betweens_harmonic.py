# before_betweens_harmonic.py, initialization for before_betweens_dmv.py

# todo: remove old initialization and make initialization2 use
# DMV_Grammar instead of DMV_Grammar2

from before_betweens_dmv import * # better way to do this?

# todo: tweak this
HARMONIC_C = 0.0
FNONSTOP_MIN = 25
FSTOP_MIN = 5

##############################
#      Initialization        #
##############################
def taglist(corpus):
    '''sents is of this form:
[['tag', ...], ['tag2', ...], ...]

Return a list of the tags. (Has to be ordered for enumerating to be
consistent.)

Fortunately only has to run once.
'''
    tagset = set()
    for sent in corpus:
        for tag in sent:
            tagset.add(tag)
    if 'ROOT' in tagset:
        raise ValueError("it seems we must have a new ROOT symbol")
    return list(tagset)





def init_zeros(tags):
    '''Return a frequency dictionary with DMV-relevant keys set to 0 or
    {}.

    Todo: tweak (especially for f_STOP).'''
    f = {} 
    for tag in tags:
        f['ROOT', tag] = 0
        f['sum', 'ROOT'] = 0
        for dir in [LEFT, RIGHT]:
            for adj in [ADJ, NON]:
                f[tag, 'STOP', dir, adj] = FSTOP_MIN
                f[tag, '-STOP', dir, adj] = FNONSTOP_MIN
        f[tag, RIGHT] = {}
        f[tag, LEFT] = {}
        f[tag, 'sum', RIGHT] = 0.0
        f[tag, 'sum', LEFT] = 0.0
    return f

def init_freq(corpus, tags):
    '''Returns f, a dictionary with these types of keys:
    - ('ROOT', tag) is basically just the frequency of tag
    - (tag, 'STOP', 'LN') is for P_STOP(STOP|tag, left, non_adj);
      etc. for 'RN', 'LA', 'LN', '-STOP'.
    - (tag, LEFT) is a dictionary of arg:f, where head could take arg
      to direction LEFT (etc. for RIGHT) and f is "harmonically" divided
      by distance, used for finding P_CHOOSE

      Does this stuff:
      1. counts word frequencies for f_ROOT
      2. adds to certain f_STOP counters if a word is found first,
         last, first or second, or last or second to last in the sentence
         (Left Adjacent, Left Non-Adjacent, etc)
      3. adds to f_CHOOSE(arg|head) a "harmonic" number (divided by
         distance between arg and head)
    '''
    f = init_zeros(tags)
    
    for sent in corpus: # sent is ['VBD', 'NN', ...]
        n = len(sent)
        # NOTE: head in DMV_Rule is a number, while this is the string
        for loc_h, head in enumerate(sent): 
            # todo grok: how is this different from just using straight head
            # frequency counts, for the ROOT probabilities?
            f['ROOT', head] += 1
            f['sum', 'ROOT'] += 1
            
            # True = 1, False = 0. todo: make prettier
            f[head,  'STOP',  LEFT,NON] += (loc_h == 1)     # second word
            f[head, '-STOP',  LEFT,NON] += (not loc_h == 1) # not second
            f[head,  'STOP',  LEFT,ADJ] += (loc_h == 0)     # first word
            f[head, '-STOP',  LEFT,ADJ] += (not loc_h == 0) # not first
            f[head,  'STOP', RIGHT,NON] += (loc_h == n - 2)     # second-to-last
            f[head, '-STOP', RIGHT,NON] += (not loc_h == n - 2) # not second-to-last
            f[head,  'STOP', RIGHT,ADJ] += (loc_h == n - 1)     # last word
            f[head, '-STOP', RIGHT,ADJ] += (not loc_h == n - 1) # not last
            
            # this is where we make the "harmonic" distribution. quite.
            for loc_a, arg in enumerate(sent):
                if loc_h != loc_a:
                    harmony = 1.0/abs(loc_h - loc_a) + HARMONIC_C
                    if loc_h > loc_a:
                        dir = LEFT
                    else:
                        dir = RIGHT
                    if arg not in f[head, dir]:
                        f[head, dir][arg] = 0.0
                    f[head, dir][arg] += harmony
                    f[head, 'sum', dir] += harmony
                # todo, optimization: possible to do both directions
                # at once here, and later on rule out the ones we've
                # done? does it actually speed things up?

    return f 

def init_normalize(f, tags, numtag, tagnum):
    '''Use frequencies (and sums) in f to return create p_STOP and
    p_ATTACH; at the same time adding the context-free rules to the
    grammar using these probabilities. 

    Return a usable grammar.'''
    p_rules = []
    p_STOP, p_ROOT, p_ATTACH, p_terminals = {},{},{},{}
    for n_h, head in numtag.iteritems():
        p_ROOT[n_h] = float(f['ROOT', head]) / f['sum', 'ROOT']
        p_rules.append( DMV_Rule(ROOT, STOP, (SEAL,n_h),
                                 p_ROOT[n_h],
                                 p_ROOT[n_h]))
        
        # p_STOP = STOP / (STOP + NOT_STOP)
        for dir in [LEFT,RIGHT]:
            for adj in [NON,ADJ]:
                p_STOP[n_h, dir, adj] = \
                    float(f[head, 'STOP', dir, adj]) / \
                    (f[head, 'STOP', dir, adj] + f[head, '-STOP', dir, adj])
        # make rule using the previously found probN and probA:
        p_rules.append( DMV_Rule((RGOL, n_h), (GOR, n_h), STOP,
                                 p_STOP[n_h, RIGHT,NON],
                                 p_STOP[n_h, RIGHT,ADJ]) )
        p_rules.append( DMV_Rule((SEAL, n_h), STOP, (RGOL, n_h),
                                 p_STOP[n_h, LEFT,NON],
                                 p_STOP[n_h, LEFT,ADJ]) )
            
        p_terminals[(GOR, n_h), head] = 1.0
        # inner() shouldn't have to deal with those long non-branching
        # stops. But actually, since these are added rules they just
        # make things take more time: 2.77s with, 1.87s without
        # p_terminals[(RGOL, n_h), head] = p_STOP[n_h, 'RA']
        # p_terminals[(SEAL, n_h), head] = p_STOP[n_h, 'RA'] * p_STOP[n_h, 'LA']
        
        for dir in [LEFT, RIGHT]:
            for arg, val in f[head, dir].iteritems():
                p_ATTACH[tagnum[arg], n_h, dir] = float(val) / f[head,'sum',dir]
    
    # after the head tag-loop, add every head-argument rule:
    for (n_a, n_h, dir),p_A in p_ATTACH.iteritems():
        if dir == LEFT: # arg is to the left of head
            p_rules.append( DMV_Rule((RGOL,n_h), (SEAL,n_a), (RGOL,n_h),
                                     p_A*(1-p_STOP[n_h, dir, NON]),
                                     p_A*(1-p_STOP[n_h, dir, ADJ])) )
        if dir == RIGHT: 
            p_rules.append( DMV_Rule((GOR,n_h), (GOR,n_h), (SEAL,n_a),
                                     p_A*(1-p_STOP[n_h, dir, NON]),
                                     p_A*(1-p_STOP[n_h, dir, ADJ])) )

    return DMV_Grammar(numtag, tagnum, p_rules, p_terminals, p_STOP, p_ATTACH, p_ROOT)

def initialize(corpus):
    '''Return an initialized DMV_Grammar
    corpus is a list of lists of tags.'''
    tags = taglist(corpus)
    numtag, tagnum = {}, {}
    for num, tag in enumerate(tags):
        tagnum[tag] = num
        numtag[num] = tag
    # f: frequency counts used in initialization, mostly distances
    f = init_freq(corpus, tags)
    g = init_normalize(f, tags, numtag, tagnum)
    return g


if __name__ == "__main__":
    # todo: grok why there's so little difference in probN and probA values

    print "--------initialization testing------------"
    print initialize([['foo', 'two','foo','foo'],
                      ['zero', 'one','two','three']])
    
    for (n,s) in [(95,5),(5,5)]:
        FNONSTOP_MIN = n
        FSTOP_MIN = s
        
        testcorpus = [s.split() for s in ['det nn vbd c nn vbd nn','det nn vbd c nn vbd pp nn',
                                          'det nn vbd nn','det nn vbd c nn vbd pp nn', 
                                          'det nn vbd nn','det nn vbd c nn vbd pp nn', 
                                          'det nn vbd nn','det nn vbd c nn vbd pp nn', 
                                          'det nn vbd nn','det nn vbd c nn vbd pp nn', 
                                          'det nn vbd pp nn','det nn vbd det nn', ]]
        g = initialize(testcorpus)

        stopn, nstopn,nstopa, stopa, rewriten, rewritea = 0.0, 0.0, 0.0, 0.0,0.0,0.0
        for r in g.all_rules():
            if r.L() == STOP or r.R() == STOP:
                stopn += r.probN
                nstopa += 1-r.probA
                nstopn += 1-r.probN
                stopa += r.probA
            else:
                rewriten += r.probN
                rewritea += r.probA
        print "sn:%.2f (nsn:%.2f) sa:%.2f (nsa:%.2f) rn:%.2f ra:%.2f" % (stopn, nstopn, stopa,nstopa, rewriten, rewritea)
        
    
    

def tagset_brown():
    "472 tags, takes a while to extract with tagset(), hardcoded here."
    return set(['BEDZ-NC', 'NP$', 'AT-TL', 'CS', 'NP+HVZ', 'IN-TL-HL', 'NR-HL', 'CC-TL-HL', 'NNS$-HL', 'JJS-HL', 'JJ-HL', 'WRB-TL', 'JJT-TL', 'WRB', 'DOD*', 'BER*-NC', ')-HL', 'NPS$-HL', 'RB-HL', 'FW-PPSS', 'NP+HVZ-NC', 'NNS$', '--', 'CC-TL', 'FW-NN-TL', 'NP-TL-HL', 'PPSS+MD', 'NPS', 'RBR+CS', 'DTI', 'NPS-TL', 'BEM', 'FW-AT+NP-TL', 'EX+BEZ', 'BEG', 'BED', 'BEZ', 'DTX', 'DOD*-TL', 'FW-VB-NC', 'DTS', 'DTS+BEZ', 'QL-HL', 'NP$-TL', 'WRB+DOD*', 'JJR+CS', 'NN+MD', 'NN-TL-HL', 'HVD-HL', 'NP+BEZ-NC', 'VBN+TO', '*-TL', 'WDT-HL', 'MD', 'NN-HL', 'FW-BE', 'DT$', 'PN-TL', 'DT-HL', 'FW-NR-TL', 'VBG', 'VBD', 'VBN', 'DOD', 'FW-VBG-TL', 'DOZ', 'ABN-TL', 'VB+JJ-NC', 'VBZ', 'RB+CS', 'FW-PN', 'CS-NC', 'VBG-NC', 'BER-HL', 'MD*', '``', 'WPS-TL', 'OD-TL', 'PPSS-HL', 'PPS+MD', 'DO*', 'DO-HL', 'HVG-HL', 'WRB-HL', 'JJT', 'JJS', 'JJR', 'HV+TO', 'WQL', 'DOD-NC', 'CC-HL', 'FW-PPSS+HV', 'FW-NP-TL', 'MD+TO', 'VB+IN', 'JJT-NC', 'WDT+BEZ-TL', '---HL', 'PN$', 'VB+PPO', 'BE-TL', 'VBG-TL', 'NP$-HL', 'VBZ-TL', 'UH', 'FW-WPO', 'AP+AP-NC', 'FW-IN', 'NRS-TL', 'ABL', 'ABN', 'TO-TL', 'ABX', '*-HL', 'FW-WPS', 'VB-NC', 'HVD*', 'PPS+HVD', 'FW-IN+AT', 'FW-NP', 'QLP', 'FW-NR', 'FW-NN', 'PPS+HVZ', 'NNS-NC', 'DT+BEZ-NC', 'PPO', 'PPO-NC', 'EX-HL', 'AP$', 'OD-NC', 'RP', 'WPS+BEZ', 'NN+BEZ', '.-TL', ',', 'FW-DT+BEZ', 'RB', 'FW-PP$-NC', 'RN', 'JJ$-TL', 'MD-NC', 'VBD-NC', 'PPSS+BER-N', 'RB+BEZ-NC', 'WPS-HL', 'VBN-NC', 'BEZ-HL', 'PPL-NC', 'BER-TL', 'PP$$', 'NNS+MD', 'PPS-NC', 'FW-UH-NC', 'PPS+BEZ-NC', 'PPSS+BER-TL', 'NR-NC', 'FW-JJ', 'PPS+BEZ-HL', 'NPS$', 'RB-TL', 'VB-TL', 'BEM*', 'MD*-HL', 'FW-CC', 'NP+MD', 'EX+HVZ', 'FW-CD', 'EX+HVD', 'IN-HL', 'FW-CS', 'JJR-HL', 'FW-IN+NP-TL', 'JJ-TL-HL', 'FW-UH', 'EX', 'FW-NNS-NC', 'FW-JJ-NC', 'VBZ-HL', 'VB+RP', 'BEZ-NC', 'PPSS+HV-TL', 'HV*', 'IN', 'PP$-NC', 'NP-NC', 'BEN', 'PP$-TL', 'FW-*-TL', 'FW-OD-TL', 'WPS', 'WPO', 'MD+PPSS', 'WDT+BER', 'WDT+BEZ', 'CD-HL', 'WDT+BEZ-NC', 'WP$', 'DO+PPSS', 'HV-HL', 'DT-NC', 'PN-NC', 'FW-VBZ', 'HVD', 'HVG', 'NN+BEZ-TL', 'HVZ', 'FW-VBD', 'FW-VBG', 'NNS$-TL', 'JJ-TL', 'FW-VBN', 'MD-TL', 'WDT+DOD', 'HV-TL', 'NN-TL', 'PPSS', 'NR$', 'BER', 'FW-VB', 'DT', 'PN+BEZ', 'VBG-HL', 'FW-PPL+VBZ', 'FW-NPS-TL', 'RB$', 'FW-IN+NN', 'FW-CC-TL', 'RBT', 'RBR', 'PPS-TL', 'PPSS+HV', 'JJS-TL', 'NPS-HL', 'WPS+BEZ-TL', 'NNS-TL-HL', 'VBN-TL-NC', 'QL-TL', 'NN+NN-NC', 'JJR-TL', 'NN$-TL', 'FW-QL', 'IN-TL', 'BED-NC', 'NRS', '.-HL', 'QL', 'PP$-HL', 'WRB+BER', 'JJ', 'WRB+BEZ', 'NNS$-TL-HL', 'PPSS+BEZ', '(', 'PPSS+BER', 'DT+MD', 'DOZ-TL', 'PPSS+BEM', 'FW-PP$', 'RB+BEZ-HL', 'FW-RB+CC', 'FW-PPS', 'VBG+TO', 'DO*-HL', 'NR+MD', 'PPLS', 'IN+IN', 'BEZ*', 'FW-PPL', 'FW-PPO', 'NNS-HL', 'NIL', 'HVN', 'PPSS+BER-NC', 'AP-TL', 'FW-DT', '(-HL', 'DTI-TL', 'JJ+JJ-NC', 'FW-RB', 'FW-VBD-TL', 'BER-NC', 'NNS$-NC', 'JJ-NC', 'NPS$-TL', 'VB+VB-NC', 'PN', 'VB+TO', 'AT-TL-HL', 'BEM-NC', 'PPL-TL', 'ABN-HL', 'RB-NC', 'DO-NC', 'BE-HL', 'WRB+IN', 'FW-UH-TL', 'PPO-HL', 'FW-CD-TL', 'TO-HL', 'PPS+BEZ', 'CD$', 'DO', 'EX+MD', 'HVZ-TL', 'TO-NC', 'IN-NC', '.', 'WRB+DO', 'CD-NC', 'FW-PPO+IN', 'FW-NN$-TL', 'WDT+BEZ-HL', 'RP-HL', 'CC', 'NN+HVZ-TL', 'FW-NNS-TL', 'DT+BEZ', 'WPS+HVZ', 'BEDZ*', 'NP-TL', ':-TL', 'NN-NC', 'WPO-TL', 'QL-NC', 'FW-AT+NN-TL', 'WDT+HVZ', '.-NC', 'FW-DTS', 'NP-HL', ':-HL', 'RBR-NC', 'OD-HL', 'BEDZ-HL', 'VBD-TL', 'NPS-NC', ')', 'TO+VB', 'FW-IN+NN-TL', 'PPL', 'PPS', 'PPSS+VB', 'DT-TL', 'RP-NC', 'VB', 'FW-VB-TL', 'PP$', 'VBD-HL', 'DTI-HL', 'NN-TL-NC', 'PPL-HL', 'DOZ*', 'NR-TL', 'WRB+MD', 'PN+HVZ', 'FW-IN-TL', 'PN+HVD', 'BEN-TL', 'BE', 'WDT', 'WPS+HVD', 'DO-TL', 'FW-NN-NC', 'WRB+BEZ-TL', 'UH-TL', 'JJR-NC', 'NNS', 'PPSS-NC', 'WPS+BEZ-NC', ',-TL', 'NN$', 'VBN-TL-HL', 'WDT-NC', 'OD', 'FW-OD-NC', 'DOZ*-TL', 'PPSS+HVD', 'CS-TL', 'WRB+DOZ', 'CC-NC', 'HV', 'NN$-HL', 'FW-WDT', 'WRB+DOD', 'NN+HVZ', 'AT-NC', 'NNS-TL', 'FW-BEZ', 'CS-HL', 'WPO-NC', 'FW-BER', 'NNS-TL-NC', 'BEZ-TL', 'FW-IN+AT-T', 'ABN-NC', 'NR-TL-HL', 'BEDZ', 'NP+BEZ', 'FW-AT-TL', 'BER*', 'WPS+MD', 'MD-HL', 'BED*', 'HV-NC', 'WPS-NC', 'VBN-HL', 'FW-TO+VB', 'PPSS+MD-NC', 'HVZ*', 'PPS-HL', 'WRB-NC', 'VBN-TL', 'CD-TL-HL', ',-NC', 'RP-TL', 'AP-HL', 'FW-HV', 'WQL-TL', 'FW-AT', 'NN', 'NR$-TL', 'VBZ-NC', '*', 'PPSS-TL', 'JJT-HL', 'FW-NNS', 'NP', 'UH-HL', 'NR', ':', 'FW-NN$', 'RP+IN', ',-HL', 'JJ-TL-NC', 'AP-NC', '*-NC', 'VB-HL', 'HVZ-NC', 'DTS-HL', 'FW-JJT', 'FW-JJR', 'FW-JJ-TL', 'FW-*', 'RB+BEZ', "''", 'VB+AT', 'PN-HL', 'PPO-TL', 'CD-TL', 'UH-NC', 'FW-NN-TL-NC', 'EX-NC', 'PPSS+BEZ*', 'TO', 'WDT+DO+PPS', 'IN+PPO', 'AP', 'AT', 'DOZ-HL', 'FW-RB-TL', 'CD', 'NN+IN', 'FW-AT-HL', 'PN+MD', "'", 'FW-PP$-TL', 'FW-NPS', 'WDT+BER+PP', 'NN+HVD-TL', 'MD+HV', 'AT-HL', 'FW-IN+AT-TL'])
