

import timeit
def adj_f(middle, loc_h):
    return middle+.5 == loc_h or middle-.5 == loc_h
def adj_f2(middle, loc_h):
    return middle-1 < loc_h < middle+1
def adj_p(middle, loc_h):
    return middle == loc_h[0] or middle == loc_h[1]

print "float:"
print timeit.Timer("junk.adj_f(1,0.5)","import junk").timeit(1000000)
print "pair:"
print timeit.Timer("junk.adj_p(1,[0,1])","import junk").timeit(1000000)
    
print "float2:"
print timeit.Timer("junk.adj_f2(1,0.5)","import junk").timeit(1000000)

# todo: some more testing on the Brown corpus:
#     # first five sentences of the Brown corpus:
#     g_brown = harmonic.initialize([['AT', 'NP-TL', 'NN-TL', 'JJ-TL', 'NN-TL', 'VBD', 'NR', 'AT', 'NN', 'IN', 'NP$', 'JJ', 'NN', 'NN', 'VBD', '``', 'AT', 'NN', "''", 'CS', 'DTI', 'NNS', 'VBD', 'NN', '.'], ['AT', 'NN', 'RBR', 'VBD', 'IN', 'NN', 'NNS', 'CS', 'AT', 'NN-TL', 'JJ-TL', 'NN-TL', ',', 'WDT', 'HVD', 'JJ', 'NN', 'IN', 'AT', 'NN', ',', '``', 'VBZ', 'AT', 'NN', 'CC', 'NNS', 'IN', 'AT', 'NN-TL', 'IN-TL', 'NP-TL', "''", 'IN', 'AT', 'NN', 'IN', 'WDT', 'AT', 'NN', 'BEDZ', 'VBN', '.'], ['AT', 'NP', 'NN', 'NN', 'HVD', 'BEN', 'VBN', 'IN', 'NP-TL', 'JJ-TL', 'NN-TL', 'NN-TL', 'NP', 'NP', 'TO', 'VB', 'NNS', 'IN', 'JJ', '``', 'NNS', "''", 'IN', 'AT', 'JJ', 'NN', 'WDT', 'BEDZ', 'VBN', 'IN', 'NN-TL', 'NP', 'NP', 'NP', '.'], ['``', 'RB', 'AT', 'JJ', 'NN', 'IN', 'JJ', 'NNS', 'BEDZ', 'VBN', "''", ',', 'AT', 'NN', 'VBD', ',', '``', 'IN', 'AT', 'JJ', 'NN', 'IN', 'AT', 'NN', ',', 'AT', 'NN', 'IN', 'NNS', 'CC', 'AT', 'NN', 'IN', 'DT', 'NN', "''", '.'], ['AT', 'NN', 'VBD', 'PPS', 'DOD', 'VB', 'CS', 'AP', 'IN', 'NP$', 'NN', 'CC', 'NN', 'NNS', '``', 'BER', 'JJ', 'CC', 'JJ', 'CC', 'RB', 'JJ', "''", '.'], ['PPS', 'VBD', 'CS', 'NP', 'NNS', 'VB', '``', 'TO', 'HV', 'DTS', 'NNS', 'VBN', 'CC', 'VBN', 'IN', 'AT', 'NN', 'IN', 'VBG', 'CC', 'VBG', 'PPO', "''", '.'], ['AT', 'JJ', 'NN', 'VBD', 'IN', 'AT', 'NN', 'IN', 'AP', 'NNS', ',', 'IN', 'PPO', 'AT', 'NP', 'CC', 'NP-TL', 'NN-TL', 'VBG', 'NNS', 'WDT', 'PPS', 'VBD', '``', 'BER', 'QL', 'VBN', 'CC', 'VB', 'RB', 'VBN', 'NNS', 'WDT', 'VB', 'IN', 'AT', 'JJT', 'NN', 'IN', 'ABX', 'NNS', "''", '.'], ['NN-HL', 'VBN-HL'], ['WRB', ',', 'AT', 'NN', 'VBD', 'PPS', 'VBZ', '``', 'DTS', 'CD', 'NNS', 'MD', 'BE', 'VBN', 'TO', 'VB', 'JJR', 'NN', 'CC', 'VB', 'AT', 'NN', 'IN', 'NN', "''", '.'], ['AT', 'NN-TL', 'VBG-TL', 'NN-TL', ',', 'AT', 'NN', 'VBD', ',', '``', 'BEZ', 'VBG', 'IN', 'VBN', 'JJ', 'NNS', 'CS', 'AT', 'NN', 'IN', 'NN', 'NNS', 'NNS', "''", '.']])
#     # 36:'AT' in g_brown.numtag, 40:'NP-TL'
    
#     DEBUG = []
#     test_brown = inner(0,2, (LRBAR,36), g_brown, ['AT', 'NP-TL' ,'NN-TL','JJ-TL'], {})
#     if 1 in DEBUG:
#         for r in  g_brown.rules((2,36)) + g_brown.rules((1,36)) + g_brown.rules((0,36)):
#             L = r.L()
#             R = r.R()
#             if head(L) in [36,40,-2] and head(R) in [36,40,-2]:
#                 print r
#     print "Brown-test gives: %.8f" % test_brown
    
    
    
    # this will give the tag sequences of all the 6218 Brown corpus
    # sentences of length < 7:
    # [[tag for (w, tag) in sent]
    #  for sent in nltk.corpus.brown.tagged_sents() if len(sent) < 7]



# ##############################
# #          from dmv          #
# ##############################
# def prune2(s,t,LHS,loc_h, ichart,tree):
#     '''TODO '''
#     newichart = {}
#     def prune2_helper(s,t,LHS,loc_h):
#         newichart[(s,t,LHS,loc_h)] = ichart[(s,t,LHS,loc_h)]
#         for d in tree[s,t,LHS,loc_h]:
#             prune2_helper(d[0],d[1],d[2],d[3])
    
#     prune2_helper(s,t,LHS,loc_h)
#     return newichart

# def prune(s,t,LHS, g, sent_nums, ichart):
#     '''Removes unused subtrees with positive probability from the
#     ichart.

#     Unused := any and all mothers (or grandmothers etc.) have
#     probability 0.0'''
#     def prune_helper(keep,s,t,LHS,loc_h):
#         keep = keep and ichart[(s,t,LHS,loc_h)] > 0.0
#         for rule in g.sent_rules(LHS, sent_nums):
#             L = rule.L()
#             R = rule.R()
#             if R==STOP:
#                 if (s,t,L,loc_h) in ichart:
#                     prune_helper(keep, s,t, L,loc_h)
#             elif L==STOP:
#                 if (s,t,R,loc_h) in ichart:
#                     prune_helper(keep, s,t, R,loc_h)
#             else:
#                 for r in xrange(s,t):
#                     for loc_L in locs(head(L), sent_nums, s, r):
#                         if (s,r,rule.L(),loc_L) in ichart:
#                             prune_helper(keep, s ,r,rule.L(),loc_L)
#                     for loc_R in locs(head(R), sent_nums, r+1, t):
#                         if (r+1,t,rule.R(),loc_R) in ichart:
#                             prune_helper(keep,r+1,t,rule.R(),loc_R)
                            
#         if not (s,t,LHS,loc_h) in keepichart:
#             keepichart[(s,t,LHS,loc_h)] = keep
#         else: # eg. if previously some parent rule had 0.0, but then a
#               # later rule said "No, I've got a use for this subtree"
#             keepichart[(s,t,LHS,loc_h)] += keep
#         return None
    
#     keepichart = {}
#     for loc_h,h in enumerate(sent_nums):
#         keep = ichart[(s,t,LHS,loc_h)] > 0.0
#         keepichart[(s,t,LHS,loc_h)] = keep
#         prune_helper(keep,s,t,LHS,loc_h)
        
#     for (s,t,LHS,loc_h),v in keepichart.iteritems():
#         if not v:
#             if  'PRUNE' in DEBUG:
#                 print "popping s:%d t:%d LHS:%s loc_h:%d" % (s,t,LHS,loc_h)
#             ichart.pop((s,t,LHS,loc_h))
# # end prune(s,t,LHS,loc_h, g, sent_nums, ichart)

# def prune_sent(g, sent_nums, ichart):
#     return prune(0, len(sent_nums)-1, ROOT, g, sent_nums, ichart)



# def P_STOP(STOP, h, dir, adj, g, corpus):
#     P_STOP_num = 0
#     P_STOP_den = 0
#     h_tag = g.numtag(h)
#     for sent in corpus:
#         chart = {} 
#         locs_h = locs(h_tag, sent)
#         io.debug( "locs_h:%s, sent:%s"%(locs_h,sent) , 'PSTOP')
#         for loc_h in locs_h:
#             inner(0, len(sent)-1, ROOT, loc_h, g, sent, chart)
#             for s in range(loc_h): # s<loc(h), range gives strictly less
#                 for t in range(loc_h, len(sent)):
#                     io.debug( "s:%s t:%s loc:%d"%(s,t,loc_h)  , 'PSTOP')
#                     if (s, t, (LRBAR,h), loc_h) in chart:
#                         io.debug( "num+=%s"%chart[(s, t, (LRBAR,h), loc_h)]  , 'PSTOP')
#                         P_STOP_num += chart[(s, t, (LRBAR,h), loc_h)]
#                     if (s, t, (RBAR,h), loc_h) in chart:
#                         io.debug( "den+=%s"%chart[(s, t, (RBAR,h), loc_h)]  , 'PSTOP')
#                         P_STOP_den += chart[(s, t, (RBAR,h), loc_h)]
                      
#     io.debug( "num/den: %s / %s"%(P_STOP_num, P_STOP_den) , 'PSTOP') 
#     if P_STOP_den > 0.0:
#         io.debug( "num/den: %s / %s = %s"%(P_STOP_num, P_STOP_den,P_STOP_num / P_STOP_den) , 'PSTOP')
#         return P_STOP_num / P_STOP_den # upside down in article
#     else:
#         return 0.0


# def DMV(sent, g):
#     '''Here it seems like they store rule information on a per-head (per
#     direction) basis, in deps_D(h, dir) which gives us a list. '''
#     def P_h(h):
#         P_h = 1 # ?
#         for dir in ['l', 'r']:
#             for a in deps(h, dir):
#                 # D(a)??
#                 P_h *= \
#                     P_STOP (0, h, dir, adj) * \
#                     P_CHOOSE (a, h, dir) * \
#                     P_h(D(a)) * \
#                 P_STOP (STOP | h, dir, adj)
#         return P_h
#     return P_h(root(sent))


# if __name__ == "__main__": # from dmv.py
#     # these are not Real rules, just testing the classes. todo: make
#     # a rule-set to test inner() on.
#     b = {}
#     s   = DMV_Rule((LRBAR,0), (NOBAR,1),(NOBAR,2), 1.0, 0.0) # s->np vp
#     np  = DMV_Rule((NOBAR,1), (NOBAR,3),(NOBAR,4), 0.3, 0.0) # np->n p
#     b[(NOBAR,1), 'n'] = 0.7 # np->'n'
#     b[(NOBAR,3), 'n'] = 1.0 # n->'n'
#     b[(NOBAR,4), 'p'] = 1.0 # p->'p'
#     vp  = DMV_Rule((NOBAR,2), (NOBAR,5),(NOBAR,1), 0.1, 0.0) # vp->v np (two parses use this rule)
#     vp2 = DMV_Rule((NOBAR,2), (NOBAR,2),(NOBAR,4), 0.9, 0.0) # vp->vp p
#     b[(NOBAR,5), 'v'] = 1.0 # v->'v'
    
#     g = DMV_Grammar([s,np,vp,vp2], b, "todo","todo", "todo")
    
#     DEBUG = 0
#     test1 = io.inner(0,0, (NOBAR,1), g, ['n'], {})
#     if test1[0] != 0.7:
#         print "should be 0.70 : %.2f" % test1[0]
#         print ""
    
#     test2 = io.inner(0,2, (NOBAR,2), g, ['v','n','p'], {})
#     if "%.2f" % test2[0] != "0.09": # 0.092999 etc, don't care about that
#         print "should be 0.09 if the io.py-test is right : %.2f" % test2[0]
#     # the following should manage to look stuff up in the chart:
#     test2 = io.inner(0,2, (NOBAR,2), g, ['v','n','p'], test2[1])
#     if "%.2f" % test2[0] != "0.09":
#         print "should be 0.09 if the io.py-test is right : %.2f" % test2[0]
