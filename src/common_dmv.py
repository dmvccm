# common-dmv.py, stuff common to cnf_ and loc_h_-files

# non-tweakable/constant "lookup" globals

LEFT, RIGHT, ADJ, NON = 0, 1, True, False 


GOR = 0  # (RIGHT,0) as an alternate possibility
RGOL = 1 # (LEFT, 1)
SEAL = 2 # 2
GOL = 3  # (LEFT, 0)
LGOR = 4 # (RIGHT,1)
SEALS = [GOR, RGOL, SEAL, GOL, LGOR]

# CNF_DMV uses these:
NGOR = 5  # we don't care about left-first attachment,
NRGOL = 6 # but have to add the non-adjacent info

ROOTNUM = -1
MPPROOT = ('ROOT', ROOTNUM)
ROOT = (SEAL, ROOTNUM) # rather arbitrary, doesn't actually make a difference. 
STOP = (GOR, -2)  

DEBUG = set(['TODO'])

def dirseal(dir): 
    if dir == LEFT:
        return (GOL, RGOL)
    elif dir == RIGHT:
        return (GOR, LGOR)
    else:
        raise ValueError

def xgo_left(gt, lteq): # i < k <= loc_l(h) 
    return xrange(gt+1, lteq+1)

def xgo_right(gt, lt): # loc_l(h) < k < j (but loc_r(h) <= k)
    return xrange(gt+1, lt)

def xlteq(lteq):
    return xrange(0,lteq+1)

def xlt(lt):
    return xrange(0,lt)

def xgt(gt, sent):
    '''Include the position "behind" the last word, ie. len(sent).'''
    lt = len(sent) + 1 
    return xrange(gt+1, lt) 

def xeq(eq, sent=[]):
    return [eq]

def xgteq(gteq, sent):
    '''Include the position "behind" the last word, ie. len(sent).'''
    lt = len(sent) + 1 
    return xrange(gteq, lt)

def xtween(gt, lt): 
    ''' For use with eg k:i<k<loc_h. Makes sure we get all and only the
    integers between gt and lt:
    
    >>> [x for x in xtween(3,7)]
    [4, 5, 6]
    >>> [x for x in xtween(3.5,7)]
    [4, 5, 6]
    >>> [x for x in xtween(3.5,6.5)]
    [4, 5, 6]    '''
    return xrange(gt+1, lt)

def seals(node):
    return node[0]

def POS(node):
    "Actually just a number representing the POS-tag"
    return node[1]

def node_str(node, tag=lambda x:x):
    if(node == ROOT):
        return 'ROOT '
    elif(node == STOP):
        return 'STOP '
    else:
        s_h = seals(node)
        h = POS(node)
        if(s_h == RGOL):
            return "  %s><" % tag(h)
        elif(s_h == SEAL):
            return " _%s_ " % tag(h)
        elif(s_h == GOR):
            return "  %s> " % tag(h)
        elif(s_h == GOL):
            return " <%s  " % tag(h)
        elif(s_h == LGOR):
            return "><%s  " % tag(h)
        elif(s_h == NGOR):
            return " >%s> " % tag(h)
        elif(s_h == NRGOL):
            return " <%s><" % tag(h)
        else:
            raise ValueError("in node_str, got as node: %s" % node)


def test(wanted, got, wanted_name="it", got_name=""):
    if not wanted == got:
        raise Warning, "Wanted %s to be %s, but %s was %s" % (wanted_name, wanted, got_name, got)

