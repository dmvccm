# Todo: since we evaluate _after_ we reestimate, we loose the icharts
# made while reestimating. If we had these available, evaluate and
# corpus_likelihood would be a lot faster, but since they need to run
# _after_ reestimate, we'll have to store an ichart per sentence. So
# try storing those icharts in some loc_h_dmv global, and see if it's
# faster using space rather than time.

from common_dmv import MPPROOT, test, node_str
from wsjdep import WSJDepCorpusReader

#HARMONIC_C: 509.637290698, FNONSTOP_MIN: 30.1124584139, FSTOP_MIN: 13.0830178845
def initialize_loc_h(tagonlys):
    import loc_h_harmonic # since we need to change constants (is there a better way?)
    reload(loc_h_harmonic)
    import random
#     loc_h_harmonic.HARMONIC_C = 380.111684914
#     loc_h_harmonic.FSTOP_MIN = 13.5744632704
#     loc_h_harmonic.FNONSTOP_MIN = 34.8939452454
    loc_h_harmonic.HARMONIC_C =  0.0 #120.0 * random.random() # 509.63
    loc_h_harmonic.STOP_C =      3.0 #3.0 * random.random() 
    loc_h_harmonic.NSTOP_C =     1.0 #5.0 * random.random() # 0.1
    loc_h_harmonic.FSTOP_MIN =  10.0 #20.0 * random.random() # 13.08 
    
    loc_h_harmonic.RIGHT_FIRST = 1.0
    loc_h_harmonic.OLD_STOP_CALC = False
    print '''
HARMONIC_C: %s, STOP_C: %s, NSTOP_C: %s, FSTOP_MIN: %s
RIGHT_FIRST: %s, OLD_STOP_CALC: %s'''%(loc_h_harmonic.HARMONIC_C,
                                       loc_h_harmonic.STOP_C,
                                       loc_h_harmonic.NSTOP_C,
                                       loc_h_harmonic.FSTOP_MIN,
                                       loc_h_harmonic.RIGHT_FIRST,
                                       loc_h_harmonic.OLD_STOP_CALC)
    g = loc_h_harmonic.initialize(tagonlys)
    return g

def initialize_uniform_loc_h(tagonlys):
    import loc_h_harmonic
    return loc_h_harmonic.uniform_init(tagonlys)

def initialize_cnf(tagonlys):
    import cnf_harmonic # since we need to change constants (is there a better way?)
    reload(cnf_harmonic)
    cnf_harmonic.HARMONIC_C = 0.0
    cnf_harmonic.FNONSTOP_MIN = 25
    cnf_harmonic.FSTOP_MIN = 5
    return cnf_harmonic.initialize(tagonlys)
    

def test_likelihood(reestimate, initialize, inner_sent,
                    corpus_size=20, corpus_offset=1000, iterations=4, EVAL=False):
    def run_IO(g, iterations, tagonlys, tags_and_parses):
        sumlog,msg = corpus_likelihood(g, tagonlys)
        print msg
        if EVAL:
            g.E = evaluate(g, tags_and_parses)
            print g.E
        for i in range(iterations):
            g = reestimate(g, tagonlys)
            print "reestimation number %d done\n"%i
            if EVAL:
                g.E = evaluate(g, tags_and_parses)
                print g.E
            prev_sumlog = sumlog
            sumlog,msg = corpus_likelihood(g, tagonlys)
            if sumlog < prev_sumlog:
                raise Exception, msg+"but previous was %s"%prev_sumlog
            print msg
            # since I want to be able to do stuff with it afterwards:
            from pickle import dump # let us say g = pickle.load(open('..','rb'))
            filehandler = open('current_grammar.obj','w')
            dump(g, filehandler)
            filehandler.close()
                    
        return g

    def corpus_likelihood(g, tagsonly):
        from math import log
        sumlog = 0.0
        for sent in tagsonly:
            p_sent = inner_sent(g, sent, {})
            if p_sent == 0.0:
                print "%s had zero probability!"%sent
            else:
                sumlog += log(p_sent)
        avg = sumlog / len(tagsonly)
        return (sumlog, "Sum of log P_{sentence}: %.4f (should move towards 0), avg: %s"%(sumlog,avg))

    reader = WSJDepCorpusReader(None)
    tagonlys = reader.tagonly_sents()[corpus_offset:corpus_offset+corpus_size]
    tags_and_parses = reader.tagged_and_parsed_sents()[corpus_offset:corpus_offset+corpus_size]
    
    print "\ninitializing %d sentences..." % corpus_size,
    g = initialize(tagonlys)
    print "initialized"
    
    g = run_IO(g, iterations, tagonlys, tags_and_parses) # make iterations argument, todo

    if EVAL:
        import pprint
        print "underproposed:"
        pprint.pprint(g.E.underproposed)
        print "overproposed:"
        pprint.pprint(g.E.overproposed)

    return g


class Evaluation():
    "Just a class to hold evaluation-relevant information, sum it up, and print it."
    def __init__(self):
        self.underproposed, self.overproposed = {}, {}
        self.R, self.R_r, self.P, self.P_r = {}, {}, {}, {}
        for nd in ['num', 'den']:
            self.R[nd], self.R_r[nd], self.P[nd], self.P_r[nd] = 0, 0, 0, 0

        self.unrooted = 0 # parses where we couldn't add_root
        self.double_heads = 0 # parses w/ two heads to one argument
        self._precision, self._recall, self._precision_r, self._recall_r = 0.0, 0.0, 0.0, 0.0
        self._F1, self._F1_r = 0.0, 0.0
        
    def calc_F1_P_R(self):
        "F1 = (2 * P * R)/(P + R), harmonic avg. of P and R"
        self._recall = float(self.R['num']) / float(self.R['den'])
        self._precision = float(self.P['num']) / float(self.P['den'])
        self._recall_r = float(self.R['num']+self.R_r['num']) / \
            float(self.R['den']+self.R_r['den'])
        self._precision_r = float(self.P['num']+self.P_r['num']) / \
            float(self.P['den']+self.P_r['den'])

        if (self._precision + self._recall) > 0.0:
            self._F1 = (2 * self._recall * self._precision) / (self._precision + self._recall)
        if (self._precision_r + self._recall_r) > 0.0:
            self._F1_r = (2 * self._recall_r * self._precision_r) / (self._precision_r + self._recall_r)
        
    def __str__(self):
        self.calc_F1_P_R()
        R_rnum = self.R['num']+self.R_r['num']
        R_rden = self.R['den']+self.R_r['den']
        P_rnum = self.P['num']+self.P_r['num']
        P_rden = self.P['den']+self.P_r['den']
        str_vals = (self.P['num'],self.P['den'],self._precision, P_rnum,P_rden,self._precision_r,
                    self.R['num'],self.R['den'],self._recall,    R_rnum,R_rden,self._recall_r,
                    self._F1, self._F1_r, self.unrooted, self.double_heads)
        regular_str = '''P:  %5d/%5d = %s | P_r:  %5d/%5d = %s
R:  %5d/%5d = %s | R_r:  %5d/%5d = %s
F1:               %s | F1_r:               %s (unrooted gold parses: %d, double-headed: %d)'''%str_vals

        if self._precision != self._recall: print regular_str # raise Exception todo
        
        tex_str_vals = tuple([p * 100 for p in (self._F1,self._F1_r)])
        tex_str = "$C_A=; C_S=;C_N=;C_M=$   & %.1f   & %.1f \\"%tex_str_vals

        return tex_str # todo make variable
                
        

def evaluate(g, tagged_and_parsed_sents):
    ''' tagged_and_parsed_sents is a list of pairs:
    (tagonly_sent, parsed_sent)

    R_num += 1 if pair from parsed is in mpp
    R_den += 1 per pair from parsed

    P_num += 1 if pair from mpp is in parsed
    P_den += 1 per pair from mpp '''
    from loc_h_dmv import mpp
    from wsjdep import add_root
    E = Evaluation()

    for sent, gold_parse in tagged_and_parsed_sents:
        if len(sent)-1 != len(gold_parse):
            E.double_heads += 1
            continue
        mpp_sent = mpp(g, sent)
        try: gold_parse = add_root(gold_parse)
        except RuntimeError: E.unrooted += 1

        for pair in gold_parse:
            dict = E.R
            if pair[0] == MPPROOT: dict = E.R_r
            dict['den'] += 1
            if pair in mpp_sent: dict['num'] += 1
            else:
                try: E.underproposed[pair] += 1
                except KeyError: E.underproposed[pair] = 1

        for pair in mpp_sent:
            dict = E.P
            if pair[0] == MPPROOT: dict = E.P_r
            dict['den'] += 1
            if pair in gold_parse: dict['num'] += 1
            else:
                try: E.overproposed[pair] += 1
                except KeyError: E.overproposed[pair] = 1
            
    return E



def compare_loc_h_cnf():
    reader = WSJDepCorpusReader(None)
    corpus_size = 200
    corpus_offset = 1000
    tagonlys = reader.tagonly_sents()[corpus_offset:corpus_offset+corpus_size]

    import loc_h_harmonic, cnf_harmonic
    g_l = loc_h_harmonic.initialize(tagonlys)
    g_c = cnf_harmonic.initialize(tagonlys)
    
    initials = [
         (g_l.p_ROOT.iteritems(), g_c.p_ROOT),
         (g_c.p_ROOT.iteritems(), g_l.p_ROOT),
         (g_l.p_STOP.iteritems(), g_c.p_STOP),
         (g_c.p_STOP.iteritems(), g_l.p_STOP),
         (g_l.p_ATTACH.iteritems(), g_c.p_ATTACH),
         (g_c.p_ATTACH.iteritems(), g_l.p_ATTACH)]
    for a_items, b in initials: 
        for k,v in a_items:
            if k not in b.keys(): raise Warning, "a[%s]=%s, but %s not in b"(k,v,k)
            if (k,v) not in b.iteritems(): raise Warning, "a[%s]=%s, but b[%s]=%s"(k,v,k,b[k])

    
    import loc_h_dmv, cnf_dmv
    from common_dmv import GOR
    for sent in tagonlys:
        ochart_l, ochart_c, ichart_l, ichart_c = {},{},{},{}
        i_l = loc_h_dmv.inner_sent(g_l, sent, ichart_l)
        i_c = cnf_dmv.inner_sent(g_c, sent, ichart_c)
        test( "%s"%i_l, "%s"%i_c, "i_l","i_c")
        
        for loc_w,w in enumerate(sent):
            w_node = (GOR, g_l.tagnum(w))
            o_l = loc_h_dmv.outer(loc_w,loc_w+1,w_node,loc_w, g_l, sent, ichart_l,ochart_l)
            o_c =   cnf_dmv.outer(loc_w,loc_w+1,w_node,       g_c, sent, ichart_c,ochart_c)
            print "%s, %s, %s"%(sent,node_str(w_node),loc_w)
            test("%s"%o_l, "%s"%o_c, "o_l(0,1,(GOR,%s),%d,...)"%(w,loc_w),"o_c")

# end compare_loc_h_cnf()


def init_nothing(g,H,S,N,M):
    print '''
HARMONIC_C: %s, STOP_C: %s, NSTOP_C: %s, FSTOP_MIN: %s'''%(H,S,N,M)
    return lambda corpus:g

def rnd_grammars_test():
    import loc_h_dmv
    reload(loc_h_dmv)

    rnd_grammars0 = []
    for i in xrange(20):
        g = test_likelihood(loc_h_dmv.reestimate,
                            initialize_loc_h,
                            loc_h_dmv.inner_sent,
                            corpus_size=6268,
                            iterations=0,
                            corpus_offset=0,
                            EVAL=True)
        rnd_grammars0 += [(g, g.HARMONIC_C, g.STOP_C, g.NSTOP_C, g.FSTOP_MIN)]

    rnd_grammars1 = [(test_likelihood(loc_h_dmv.reestimate,
                                      init_nothing(g,H,S,N,M),
                                      loc_h_dmv.inner_sent,
                                      corpus_size=6268,
                                      iterations=1,
                                      corpus_offset=0,
                                      EVAL=True),
                      H,S,N,M)
                    for g,H,S,N,M in rnd_grammars0]
    rnd_grammars2 = [(test_likelihood(loc_h_dmv.reestimate,
                                      init_nothing(g,H,S,N,M),
                                      loc_h_dmv.inner_sent,
                                      corpus_size=6268,
                                      iterations=1,
                                      corpus_offset=0,
                                      EVAL=True),
                      H,S,N,M)
                    for g,H,S,N,M in rnd_grammars1]


if __name__ == "__main__":
    print "main.py:"
    
    if False:
        rnd_grammars_test()
    else:
        import loc_h_dmv
        reload(loc_h_dmv)
        print "\ntrying reestimate v.1 ##############################"
        g = test_likelihood(loc_h_dmv.reestimate,
                            initialize_loc_h,
                            loc_h_dmv.inner_sent,
                            corpus_size=6268,
                            iterations=30,
                            corpus_offset=0,
                            EVAL=True)
        print g 

#         print "\ntrying reestimate v.2 ##############################"
#         g = test_likelihood(loc_h_dmv.reestimate2,
#                             initialize_loc_h,
#                             loc_h_dmv.inner_sent,
#                             corpus_size=5,
#                             iterations=4,
#                             corpus_offset=0)
#         print "main.py: done"
#         print g


#     compare_loc_h_cnf()
#     import cnf_dmv
#     reload(cnf_dmv)
#     print "\ntrying cnf-reestimate ##############################"
#     g = test_likelihood(cnf_dmv.reestimate,
#                         initialize_cnf,
#                         cnf_dmv.inner_sent,
#                         corpus_size=5,
#                         iterations=4)
