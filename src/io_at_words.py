# io_at_words.py, sentence positions at word locations

# import numpy # numpy provides Fast Arrays, for future optimization
# import pprint # for pretty-printing

DEBUG = set(['TODO'])

# some of dmv-module bleeding in here... todo: prettier (in inner())
NOBAR = 0
STOP = (NOBAR, -2) 

def debug(string, level='TODO'):
    '''Easily turn on/off inline debug printouts with this global. There's
a lot of cluttering debug statements here, todo: clean up'''
    if level in DEBUG:
        print string


class Grammar():
    '''The PCFG used in the I/O-algorithm.

    Public members:
    p_terminals
    
    Todo: as of now, this allows duplicate rules... should we check
    for this?  (eg. g = Grammar([x,x],[]) where x.prob == 1 may give
    inner probabilities of 2.)'''
    def all_rules(self):
        return self.__p_rules
    
    def rules(self, LHS):
        return [rule for rule in self.all_rules() if rule.LHS() == LHS]
    
    def nums(self):
        return self.__numtag
    
    def sent_nums(self, sent):
        return [self.tagnum(tag) for tag in sent]

    def numtag(self, num):
        return self.__numtag[num]
    
    def tagnum(self, tag):
        return self.__tagnum[tag]

    def __init__(self, p_rules, p_terminals, numtag, tagnum):
        '''rules and p_terminals should be arrays, where p_terminals are of
        the form [preterminal, terminal], and rules are CNF_Rule's.'''        
        self.__p_rules = p_rules # todo: could check for summing to 1 (+/- epsilon)
        self.__numtag = numtag
        self.__tagnum = tagnum
        self.p_terminals = p_terminals




class CNF_Rule():
    '''A single CNF rule in the PCFG, of the form 
    LHS -> L R
    where these are just integers
    (where do we save the connection between number and symbol?
    symbols being 'vbd' etc.)'''
    def __eq__(self, other):
        return self.LHS() == other.LHS() and self.R() == other.R() and self.L() == other.L()
    def __ne__(self, other):
        return self.LHS() != other.LHS() or self.R() != other.R() or self.L() != other.L()
    def __str__(self):
        return "%s -> %s %s [%.2f]" % (self.LHS(), self.L(), self.R(), self.prob)
    def __init__(self, LHS, L, R, prob):
        self.__LHS = LHS
        self.__R = R
        self.__L = L
        self.prob = prob
    def p(self, *arg):
        "Return a probability, doesn't care about attachment..."
        return self.prob
    def LHS(self):
        return self.__LHS
    def L(self):
        return self.__L
    def R(self):
        return self.__R
    
def inner(s, t, LHS, g, sent, chart):
    ''' Give the inner probability of having the node LHS cover whatever's
    between s and t in sentence sent, using grammar g.

    Returns a pair of the inner probability and the chart

    For DMV, LHS is a pair (bar, h), but this function ought to be
    agnostic about that.

    e() is an internal function, so the variable chart (a dictionary)
    is available to all calls of e().

    Since terminal probabilities are just simple lookups, they are not
    put in the chart (although we could put them in there later to
    optimize)
    '''
    
    def O(s):
        return sent[s]
    
    def e(s,t,LHS):
        '''Chart has lists of probability and whether or not we've attached
yet to L and R, each entry is a list [p, Rattach, Lattach], where if
Rattach==True then the rule has a right-attachment or there is one
lower in the tree (meaning we're no longer adjacent).'''
        if (s, t, LHS) in chart:
            return chart[s, t, LHS]
        else:
            debug( "trying from %d to %d with %s" % (s,t,LHS) )
            if s == t:
                if (LHS, O(s)) in g.p_terminals:
                    prob = g.p_terminals[LHS, O(s)] # b[LHS, O(s)]
                else:
                    prob = 0.0 # todo: is this the right way to deal with lacking rules?
                    print "\t LACKING TERMINAL:"
                debug( "\t terminal: %s -> %s : %.1f" % (LHS, O(s), prob) )
                # terminals have no attachment
                return prob
            else:
                if (s,t,LHS) not in chart:
                    # by default, not attachment yet
                    chart[s,t,LHS] = 0.0 #, False, False]
                for rule in g.rules(LHS): # summing over j,k in a[LHS,j,k]
                    debug( "\tsumming rule %s" % rule ) 
                    L = rule.L()
                    R = rule.R()
                    for r in range(s, t): # summing over r = s to r = t-1
                        p_L = e(s, r, L)
                        p_R = e(r + 1, t, R)
                        p = rule.p("todo","todo") 
                        chart[s, t, LHS] += p * p_L * p_R
                debug( "\tchart[%d,%d,%s] = %.2f" % (s,t,LHS, chart[s,t,LHS]) )
                return chart[s, t, LHS]
    # end of e-function
    
    inner_prob = e(s,t,LHS)
    if 1 in DEBUG:
        print "---CHART:---"
        for k,v in chart.iteritems():
            print "\t%s -> %s_%d ... %s_%d : %.1f" % (k[2], O(k[0]), k[0], O(k[1]), k[1], v)
        print "---CHART:end---"
    return [inner_prob, chart] 








if __name__ == "__main__":
    print "IO-module tests:"
    b = {}
    s   = CNF_Rule(0,1,2, 1.0) # s->np vp
    np  = CNF_Rule(1,3,4, 0.3) # np->n p
    b[1, 'n'] = 0.7 # np->'n'
    b[3, 'n'] = 1.0 # n->'n'
    b[4, 'p'] = 1.0 # p->'p'
    vp  = CNF_Rule(2,5,1, 0.1) # vp->v np (two parses use this rule)
    vp2 = CNF_Rule(2,2,4, 0.9) # vp->vp p
    b[5, 'v'] = 1.0 # v->'v'
    
    g = Grammar([s,np,vp,vp2], b, {0:'s',1:'np',2:'vp',3:'n',4:'p',5:'v'},
                {'s':0,'np':1,'vp':2,'n':3,'p':4,'v':5})
    
    print "The rules:"
    for i in range(0,5):
        for r in g.rules(i):
            print r
    print ""
    
    test1 = inner(0,0, 1, g, ['n'], {})
    if test1[0] != 0.7:
        print "should be 0.70 : %.2f" % test1[0]
        print ""
    
    test2 = inner(0,2, 2, g, ['v','n','p'], test1[1])
    print "should be 0.0930 : %.4f" % test2[0]
    print "------ trying the same again:----------"
    test2 = inner(0,2, 2, g, ['v','n','p'], test2[1])
    print "should be 0.0930 : %.4f" % test2[0]

    
