from nltk.corpus.reader.util import *
from nltk.corpus.reader.bracket_parse import BracketParseCorpusReader
from common_dmv import MPPROOT

class WSJDepCorpusReader(BracketParseCorpusReader):
    ''' Reader for the dependency parsed WSJ10. Will not include one-word
    sentences, since these are not parsed (and thus not
    POS-tagged!). All implemented foo_sents() functions should now be
    of length 6268 since there are 38 one-word sentences. '''
    def __init__(self, root):
        BracketParseCorpusReader.__init__(self,
                                          "../corpus/wsjdep", # path to files
                                          ['wsj.combined.10.dep']) # file-list or regexp
        
    def _read_block(self, stream):
        return read_regexp_block(stream,
                                 start_re=r'<sentence id=".+">')
    
    def _normalize(self, t):
        # convert XML to sexpr notation, more or less
        t = re.sub(r'<sentence id="10.+">', r"[ ", t)

        t = re.sub(r"\s+<text>\s+(.*)\s+</text>", r"<text>\1</text>", t)
        t = re.sub(r"<text>((.|\n)*)</text>", r"(\1)\\n", t)

        t = re.sub(r'<rel label=".*?">', r'', t)
        t = re.sub(r'\s+<head id="(\d+?)" pos="(.+?)">(.+)</head>', r'((\2 \1 \3)', t)
        t = re.sub(r'\s+<dep id="(\d+?)" pos="(.+?)">(.+)</dep>', r'(\2 \1 \3)', t)
        t = re.sub(r'\s+</rel>', r')\\n', t)

        t = re.sub(r"\s*</sentence>", r"]", t)

        # \\n means "add an \n later", since we keep removing them
        t = re.sub(r"\\n", r"\n", t) 
        return t

    def _parse(self, t):
        return dep_parse(self._normalize(t))

    def _tag(self, t): 
        tagonly = self._tagonly(t)
        tagged_sent = zip(self._word(t), tagonly)
        return tagged_sent
    
    def _word(self, t):
        PARENS = re.compile(r'\(.+\)')
        sentence = PARENS.findall(self._normalize(t))[0]
        WORD = re.compile(r'([^\s()]+)')
        words = WORD.findall(sentence)
        if len(words) < 2:
            return [] # skip one-word sentences!
        else:
            return words

    def _get_tagonly_sent(self, parse):
        "Convert dependency parse into a sorted taglist"
        if not parse:
            return None

        tagset = set([]) 
        for head, dep in parse:
            tagset.add(head)
            tagset.add(dep)
        taglist = list(tagset)
        taglist.sort(lambda x,y: x[1]-y[1])

        return [tag for tag,loc in taglist]
    
    # tags_and_parse
    def _tags_and_parse(self, t):
        parse = dep_parse(self._normalize(t))
        return (self._get_tagonly_sent(parse), parse)
    
    def _read_tags_and_parse_sent_block(self, stream):
        tags_and_parse_sents = [self._tags_and_parse(t) for t in self._read_block(stream)]
        return [(tag,parse) for (tag,parse) in tags_and_parse_sents if tag and parse]

    def tagged_and_parsed_sents(self, files=None):
        return concat([StreamBackedCorpusView(filename,
                                              self._read_tags_and_parse_sent_block)
                       for filename in self.abspaths(files)])
    
    # tagonly: 
    def _tagonly(self, t):
        parse = dep_parse(self._normalize(t))
        return self._get_tagonly_sent(parse)

    def _read_tagonly_sent_block(self, stream):
        tagonly_sents = [self._tagonly(t) for t in self._read_block(stream)]
        return [tagonly_sent for tagonly_sent in tagonly_sents if tagonly_sent]

    def tagonly_sents(self, files=None):
        return concat([StreamBackedCorpusView(filename,
                                              self._read_tagonly_sent_block)
                       for filename in self.abspaths(files)])



def dep_parse(s):
    "todo: add ROOT, which is implicitly the only non-dependent tagloc"
    def read_tagloc(pos):
        match = WORD.match(s, pos+2)
        tag = match.group(1)
        pos = match.end()
        
        match = WORD.match(s, pos)
        loc = int(match.group(1))
        pos = match.end()
        
        match = WORD.match(s, pos) # skip the actual word
        pos = match.end()
        
        return (pos,tag,loc)

    SPACE = re.compile(r'\s*')
    WORD = re.compile(r'\s*([^\s\(\)]*)\s*')
    RELSTART = re.compile(r'\(\(')

    # Skip any initial whitespace and actual sentence
    match = RELSTART.search(s, 0)
    if match:
        pos = match.start()
    else:
        # eg. one word sentence, no dependency relation
        return None
    
    parse = set([])
    head, loc_h = None, None
    while pos < len(s):
        # Beginning of a sentence
        if s[pos] == '[':
            pos = SPACE.match(s, pos+1).end()
        # End of a sentence
        if s[pos] == ']':
            pos = SPACE.match(s, pos+1).end()
            if pos != len(s): raise ValueError, "Trailing garbage following sentence"
            return parse
        # Beginning of a relation, head:
        elif s[pos:pos+2] == '((':
            pos, head, loc_h = read_tagloc(pos)
        # Dependent(s):
        elif s[pos:pos+2] == ')(':
            pos, arg, loc_a = read_tagloc(pos)
            # Each head-arg relation gets its own pair in parse,
            # although in xml we may have
            # <rel><head/><dep/><dep/><dep/></rel>
            parse.add( ((head,loc_h),(arg,loc_a)) )
        elif s[pos:pos+2] == '))':
            pos = SPACE.match(s, pos+2).end()
        else:
            print "s: %s\ns[%d]=%s"%(s,pos,s[pos])
            raise ValueError, 'unexpected token'

    print "s: %s\ns[%d]=%s"%(s,pos,s[pos])
    raise ValueError, 'mismatched parens (or something)'


def add_root(parse):
    "Return parse with ROOT added."
    rooted = None
    for (head,loc_h) in set([h for h,a in parse]):
        if (head,loc_h) not in set([a for h,a in parse]):
            if rooted:
                raise RuntimeError, "Several possible roots in parse: %s"%(list(parse),)
            else:
                rooted = (head,loc_h)

    if not rooted:
        raise RuntimeError, "No root in parse!"
    
    rootpair = (MPPROOT, rooted)
    return parse.union(set([ rootpair ]))

if __name__ == "__main__":
    print "WSJDepCorpusReader tests:"
    reader = WSJDepCorpusReader(None)

    print "Sentences:"
    print reader.sents()
    
    print "Tagged sentences:"
    print reader.tagged_sents()
    
    parsedsents = reader.parsed_sents()
#    print "Number of sentences: %d"%len(parsedsents) # takes a while
    import pprint
    print "First parsed sentence:"
    pprint.pprint(parsedsents[0])

    tags_and_parses = reader.tagged_and_parsed_sents()
    print "121st tagged and then parsed sentence:"
    pprint.pprint(tags_and_parses[121])
    
