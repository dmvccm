# loc_h_dmv.py
# 
# dmv reestimation and inside-outside probabilities using loc_h, and
# no CNF-style rules
#
# Table of Contents:
# 1. Grammar-class and related functions
# 2. P_INSIDE / inner() and inner_sent()
# 3. P_OUTSIDE / outer()
# 4. Reestimation v.1: sentences as outer loop
# 5. Reestimation v.2: head-types as outer loop
# 6. Most Probable Parse
# 7. Testing functions

import io
from common_dmv import *

### todo: debug with @accepts once in a while, but it's SLOW
# from typecheck import accepts, Any 

if __name__ == "__main__":
    print "loc_h_dmv module tests:"

def adj(middle, loc_h):  
    "middle is eg. k when rewriting for i<k<j (inside probabilities)."
    return middle == loc_h or middle == loc_h+1 # ADJ == True

def make_GO_AT(p_STOP,p_ATTACH):
    p_GO_AT = {}
    for (a,h,dir), p_ah in p_ATTACH.iteritems():
        p_GO_AT[a,h,dir, NON] = p_ah * (1-p_STOP[h, dir, NON])
        p_GO_AT[a,h,dir, ADJ] = p_ah * (1-p_STOP[h, dir, ADJ])
    return p_GO_AT

class DMV_Grammar(io.Grammar):
    def __str__(self):
        LJUST = 47
        def t(n):
            return "%d=%s" % (n, self.numtag(n))
        def p(dict,key):
            if key in dict:
                if dict[key] > 1.00000001: # stupid floating point comparisons
                    raise Exception, "probability > 1.0:%s=%s"%(key,dict[key])
                return dict[key]
            else: return 0.0
        def no_zeroL(str,tagstr,prob):
            if prob > 0.0: return (str%(tagstr,prob)).ljust(LJUST)
            else: return "".ljust(LJUST)
        def no_zeroR(str,tagstr,prob):
            if prob > 0.0: return str%(tagstr,prob)
            else: return ""
        def p_a(a,h):
            p_L = p(self.p_ATTACH,(a,h,LEFT))
            p_R = p(self.p_ATTACH,(a,h,RIGHT))
            if p_L == 0.0 and p_R == 0.0:
                return ''
            else:
                if p_L > 0.0:
                    str = "p_ATTACH[%s|%s,L] = %s" % (t(a), t(h), p_L)
                    str = str.ljust(LJUST)
                else:
                    str = ''
                if p_R > 0.0:
                    str = str.ljust(LJUST)
                    str += "p_ATTACH[%s|%s,R] = %s" % (t(a), t(h), p_R)
            return '\n'+str

        root, stop, att, ord = "","","",""
        for h in self.headnums():
            root += no_zeroL("\np_ROOT[%s] = %s", t(h), p(self.p_ROOT, (h)))
            stop += '\n'
            stop += no_zeroL("p_STOP[stop|%s,L,adj] = %s", t(h), p(self.p_STOP, (h,LEFT,ADJ)))
            stop += no_zeroR("p_STOP[stop|%s,R,adj] = %s", t(h), p(self.p_STOP, (h,RIGHT,ADJ)))
            stop += '\n'
            stop += no_zeroL("p_STOP[stop|%s,L,non] = %s", t(h), p(self.p_STOP, (h,LEFT,NON)))
            stop += no_zeroR("p_STOP[stop|%s,R,non] = %s", t(h), p(self.p_STOP, (h,RIGHT,NON)))
            att  += ''.join([p_a(a,h) for a in self.headnums()])
            ord  += '\n'
            ord  += no_zeroL("p_ORDER[ left-first|%s ] = %s", t(h), p(self.p_ORDER, (GOL,h)))
            ord  += no_zeroR("p_ORDER[right-first|%s ] = %s", t(h), p(self.p_ORDER, (GOR,h)))
        return root + stop + att + ord 

    def __init__(self, numtag, tagnum, p_ROOT, p_STOP, p_ATTACH, p_ORDER):
        io.Grammar.__init__(self, numtag, tagnum)
        self.p_ROOT = p_ROOT     # p_ROOT[w] = p
        self.p_ORDER = p_ORDER   # p_ORDER[seals, w] = p 
        self.p_STOP = p_STOP     # p_STOP[w, LEFT, NON] = p     (etc. for LA,RN,RA)
        self.p_ATTACH = p_ATTACH # p_ATTACH[a, h, LEFT] = p (etc. for R)
                                 # p_GO_AT[a, h, LEFT, NON] = p (etc. for LA,RN,RA)
        self.p_GO_AT = make_GO_AT(self.p_STOP, self.p_ATTACH)
        # these are used in reestimate2():
        self.reset_iocharts()
        
    def get_iochart(self, sent_nums):
        ch_key = tuple(sent_nums)
        try:
            ichart = self._icharts[ch_key]
        except KeyError:
            ichart = {}
        try:
            ochart = self._ocharts[ch_key]
        except KeyError:
            ochart = {}
        return (ichart, ochart)

    def set_iochart(self, sent_nums, ichart, ochart):
        self._icharts[tuple(sent_nums)] = ichart
        self._ocharts[tuple(sent_nums)] = ochart

    def reset_iocharts(self):
        self._icharts = {}
        self._ocharts = {}

    def p_GO_AT_or0(self, a, h, dir, adj):
        try:
            return self.p_GO_AT[a, h, dir, adj]
        except KeyError:
            return 0.0
    

def locs(sent_nums, start, stop):
    '''Return the between-word locations of all words in some fragment of
    sent. We make sure to offset the locations correctly so that for
    any w in the returned list, sent[w]==loc_w.
    
    start is inclusive, stop is exclusive, as in klein-thesis and
    Python's list-slicing.'''
    for i0,w in enumerate(sent_nums[start:stop]):
        loc_w = i0+start
    	yield (loc_w, w)

###################################################
#            P_INSIDE (dmv-specific)              #
###################################################

#@accepts(int, int, (int, int), int, Any(), [str], {tuple:float}, IsOneOf(None,{}))
def inner(i, j, node, loc_h, g, sent, ichart, mpptree=None):
    ''' The ichart is of this form:
    ichart[i,j,LHS, loc_h]
    where i and j are between-word positions.
    
    loc_h gives adjacency (along with k for attachment rules), and is
    needed in P_STOP reestimation.
    '''
    sent_nums = g.sent_nums(sent)

    def terminal(i,j,node, loc_h, tabs):
        if not i <= loc_h < j:
            if 'INNER' in DEBUG:
                print "%s*= 0.0 (wrong loc_h)" % tabs
            return 0.0
        elif POS(node) == sent_nums[i] and node in g.p_ORDER:
            # todo: add to ichart perhaps? Although, it _is_ simple lookup..
            prob = g.p_ORDER[node]
        else:
            if 'INNER' in DEBUG:
                print "%sLACKING TERMINAL:" % tabs
            prob = 0.0
        if 'INNER' in DEBUG:
            print "%s*= %.4f (terminal: %s -> %s_%d)" % (tabs,prob, node_str(node), sent[i], loc_h) 
        return prob
    
    def e(i,j, (s_h,h), loc_h, n_t):
        def to_mpp(p, L, R):
            if mpptree:
                key = (i,j, (s_h,h), loc_h)
                if key not in mpptree:
                    mpptree[key] = (p, L, R)
                elif mpptree[key][0] < p:
                    mpptree[key] = (p, L, R)

        def tab():
            "Tabs for debug output"
            return "\t"*n_t
        
        if (i, j, (s_h,h), loc_h) in ichart:
            if 'INNER' in DEBUG:
                print "%s*= %.4f in ichart: i:%d j:%d node:%s loc:%s" % (tab(),ichart[i, j, (s_h,h), loc_h], i, j,
                                                                       node_str((s_h,h)), loc_h)
            return ichart[i, j, (s_h,h), loc_h]
        else:
            # Either terminal rewrites, using p_ORDER:
            if i+1 == j and (s_h == GOR or s_h == GOL):
                return terminal(i, j, (s_h,h), loc_h, tab())
            else: # Or not at terminal level yet:
                if 'INNER' in DEBUG:
                    print "%s%s (%.1f) from %d to %d" % (tab(),node_str((s_h,h)),loc_h,i,j)
                if s_h == SEAL:
                    if h == POS(ROOT): # only used in testing, o/w we use inner_sent
                        h = sent_nums[loc_h]
                        if i != 0 or j != len(sent): raise ValueError
                        else: return g.p_ROOT[h] * e(i,j,(SEAL,h),loc_h,n_t+1)
                    p_RGOL = g.p_STOP[h,  LEFT, adj(i,loc_h)] * e(i,j,(RGOL,h),loc_h,n_t+1)
                    p_LGOR = g.p_STOP[h, RIGHT, adj(j,loc_h)] * e(i,j,(LGOR,h),loc_h,n_t+1)
                    p = p_RGOL + p_LGOR
                    to_mpp(p_RGOL,       STOPKEY,         (i,j, (RGOL,h),loc_h))
                    to_mpp(p_LGOR, (i,j, (RGOL,h),loc_h),       STOPKEY )
                    if 'INNER' in DEBUG:
                        print "%sp= %.4f (STOP)" % (tab(), p) 
                elif s_h == RGOL or s_h == GOL:
                    p = 0.0
                    if s_h == RGOL:
                        p = g.p_STOP[h, RIGHT, adj(j,loc_h)] * e(i,j, (GOR,h),loc_h,n_t+1)
                        to_mpp(p, (i,j, (GOR,h),loc_h), STOPKEY)
                    for k in xgo_left(i, loc_h): # i < k <= loc_l(h) 
                        p_R = e(k, j, ( s_h,h), loc_h, n_t+1)
                        if p_R > 0.0:
                            for loc_a,a in locs(sent_nums, i, k):
                                p_ah = g.p_GO_AT_or0(a, h, LEFT, adj(k,loc_h))
                                if p_ah > 0.0:
                                    p_L = e(i, k, (SEAL,a), loc_a, n_t+1)
                                    p_add = p_L * p_ah * p_R
                                    p += p_add
                                    to_mpp(p_add,
                                          (i, k, (SEAL,a), loc_a),
                                          (k, j, ( s_h,h), loc_h))
                    if 'INNER' in DEBUG:
                        print "%sp= %.4f (ATTACH)" % (tab(), p)
                elif s_h == GOR or s_h == LGOR:
                    p = 0.0
                    if s_h == LGOR:
                        p = g.p_STOP[h, LEFT, adj(i,loc_h)] * e(i,j, (GOL,h),loc_h,n_t+1)
                        to_mpp(p, (i,j, (GOL,h),loc_h), STOPKEY)
                    for k in xgo_right(loc_h, j): # loc_l(h) < k < j
                        p_L = e(i, k, ( s_h,h), loc_h, n_t+1)
                        if p_L > 0.0:
                            for loc_a,a in locs(sent_nums,k,j):
                                p_ah = g.p_GO_AT_or0(a, h, RIGHT, adj(k,loc_h))
                                p_R = e(k, j, (SEAL,a), loc_a, n_t+1)
                                p_add = p_L * p_ah * p_R
                                p += p_add
                                to_mpp(p_add,
                                      (i, k, ( s_h,h), loc_h),
                                      (k, j, (SEAL,a), loc_a))
                                
                    if 'INNER' in DEBUG:
                        print "%sp= %.4f (ATTACH)" % (tab(), p)
                # elif s_h == GOL: # todo

                ichart[i, j, (s_h,h), loc_h] = p
                return p
    # end of e-function
            
    inner_prob = e(i,j,node,loc_h, 0)
    if 'INNER' in DEBUG:
        print debug_ichart(g,sent,ichart)
    return inner_prob
# end of dmv.inner(i, j, node, loc_h, g, sent, ichart,mpptree)


def debug_ichart(g,sent,ichart):
    str = "---ICHART:---\n"
    for (s,t,LHS,loc_h),v in ichart.iteritems():
        str += "%s -> %s_%d ... %s_%d (loc_h:%s):\t%s\n" % (node_str(LHS,g.numtag),
                                                              sent[s], s, sent[s], t, loc_h, v)
    str += "---ICHART:end---\n"
    return str


def inner_sent(g, sent, ichart):
    return sum([g.p_ROOT[w] * inner(0, len(sent), (SEAL,w), loc_w, g, sent, ichart)
                for loc_w,w in locs(g.sent_nums(sent),0,len(sent))])





###################################################
#           P_OUTSIDE (dmv-specific)              #
###################################################

#@accepts(int, int, (int, int), int, Any(), [str], {tuple:float}, {tuple:float})
def outer(i,j,w_node,loc_w, g, sent, ichart, ochart):
    ''' http://www.student.uib.no/~kun041/dmvccm/DMVCCM.html#outer

    w_node is a pair (seals,POS); the w in klein-thesis is made up of
    POS(w) and loc_w
    '''
    sent_nums = g.sent_nums(sent)
    if POS(w_node) not in sent_nums[i:j]:
        # sanity check, w must be able to dominate sent[i:j]
        return 0.0

    # local functions:
    def e(i,j,LHS,loc_h): # P_{INSIDE}
        try:
            return ichart[i,j,LHS,loc_h]
        except KeyError:
            return inner(i,j,LHS,loc_h,g,sent,ichart) 

    def f(i,j,w_node,loc_w):
        if not (i <= loc_w < j):
            return 0.0 
        if (i,j,w_node,loc_w) in ochart:
            return ochart[i,j, w_node,loc_w]
        if w_node == ROOT:
            if i == 0 and j == len(sent):
                return 1.0
            else: # ROOT may only be used on full sentence
                return 0.0
        # but we may have non-ROOTs (stops) over full sentence too:
        w = POS(w_node)
        s_w = seals(w_node)

        # todo: try either if p_M > 0.0: or sum(), and speed-test them
        
        if s_w == SEAL: # w == a
            # todo: do the i<sent<j check here to save on calls?
            p = g.p_ROOT[w] * f(i,j,ROOT,loc_w)
            # left attach
            for k in xgt(j, sent): # j<k<len(sent)+1
                for loc_h,h in locs(sent_nums,j,k):
                    p_wh = g.p_GO_AT_or0(w, h, LEFT, adj(j, loc_h))
                    for s_h in [RGOL, GOL]:
                        p += f(i,k,(s_h,h),loc_h) * p_wh * e(j,k,(s_h,h),loc_h)
            # right attach
            for k in xlt(i): # k<i
                for loc_h,h in locs(sent_nums,k,i):
                    p_wh = g.p_GO_AT_or0(w, h, RIGHT, adj(i, loc_h))
                    for s_h in [LGOR, GOR]:
                        p += e(k,i,(s_h,h), loc_h) * p_wh * f(k,j,(s_h,h), loc_h)
                    
        elif s_w == RGOL or s_w == GOL: # w == h, left stop + left attach
            if s_w == RGOL:
                s_h = SEAL 
            else: # s_w == GOL
                s_h = LGOR
            p = g.p_STOP[w, LEFT,  adj(i,loc_w)] * f(i,j,( s_h,w),loc_w)
            for k in xlt(i): # k<i
                for loc_a,a in locs(sent_nums,k,i):
                    p_aw = g.p_GO_AT_or0(a, w, LEFT, adj(i, loc_w))
                    p += e(k,i, (SEAL,a),loc_a) * p_aw * f(k,j,w_node,loc_w)

        elif s_w == GOR or s_w == LGOR: # w == h, right stop + right attach
            if s_w == GOR:
                s_h = RGOL
            else: # s_w == LGOR
                s_h = SEAL
            p = g.p_STOP[w, RIGHT, adj(j,loc_w)] * f(i,j,( s_h,w),loc_w)
            for k in xgt(j, sent): # j<k<len(sent)+1
                for loc_a,a in locs(sent_nums,j,k):
                    p_ah = g.p_GO_AT_or0(a, w, RIGHT, adj(j, loc_w))
                    p += f(i,k,w_node,loc_w) * p_ah * e(j,k,(SEAL,a),loc_a)

        ochart[i,j,w_node,loc_w] = p 
        return p
    # end outer.f()
    
    return f(i,j,w_node,loc_w)
# end outer(i,j,w_node,loc_w, g,sent, ichart,ochart)




###################################################
#              Reestimation v.1:                  #
#           Sentences as outer loop               #
###################################################

def reest_zeros(h_nums):
    '''A dict to hold numerators and denominators for our 6+ reestimation
formulas. '''
    # todo: p_ORDER?
    fr = { ('ROOT','den'):0.0 } # holds sum over f_sent!! not p_sent...
    for h in h_nums:
        fr['ROOT','num',h] = 0.0 
        for s_h in [GOR,GOL,RGOL,LGOR]:
            x = (s_h,h)
            fr['hat_a','den',x] = 0.0 # = c()
            # not all arguments are attached to, so we just initialize
            # fr['hat_a','num',a,(s_h,h)] as they show up, in reest_freq
            for adj in [NON, ADJ]:
                for nd in ['num','den']:
                    fr['STOP',nd,x,adj] = 0.0
    return fr


def reest_freq(g, corpus):
    fr = reest_zeros(g.headnums())
    ichart = {}
    ochart = {}
    p_sent = None # 50 % speed increase on storing this locally

    # local functions altogether 2x faster than global
    def c(i,j,LHS,loc_h,sent):
        if not p_sent > 0.0:
            return p_sent

        p_in = e(i,j, LHS,loc_h,sent)
        if not p_in > 0.0: 
            return p_in

        p_out = f(i,j, LHS,loc_h,sent)
        return p_in * p_out / p_sent
    # end reest_freq.c()

    def f(i,j,LHS,loc_h,sent): # P_{OUTSIDE}
        try:
            return ochart[i,j,LHS,loc_h]
        except KeyError:
            return outer(i,j,LHS,loc_h,g,sent,ichart,ochart)
    # end reest_freq.f()

    def e(i,j,LHS,loc_h,sent): # P_{INSIDE}
        try:
            return ichart[i,j,LHS,loc_h]
        except KeyError:
            return inner(i,j,LHS,loc_h,g,sent,ichart) 
    # end reest_freq.e()

    def w_left(i,j, x,loc_h,sent,sent_nums): 
        if not p_sent > 0.0: return

        h = POS(x)
        a_k = {}
        for k in xtween(i, j):
            p_out = f(i,j, x,loc_h, sent)
            if not p_out > 0.0:
                continue
            p_R =   e(k,j, x,loc_h, sent)
            if not p_R > 0.0:
                continue

            for loc_a,a in locs(sent_nums, i,k): # i<=loc_l(a)<k
                p_rule = g.p_GO_AT_or0(a, h, LEFT, adj(k, loc_h))
                p_L = e(i,k, (SEAL,a), loc_a, sent)
                p = p_L * p_out * p_R * p_rule
                try:    a_k[a] += p
                except KeyError: a_k[a]  = p

        for a,p in a_k.iteritems():
            try:    fr['hat_a','num',a,x] += p / p_sent
            except KeyError: fr['hat_a','num',a,x]  = p / p_sent
    # end reest_freq.w_left() 

    def w_right(i,j, x,loc_h,sent,sent_nums):
        if not p_sent > 0.0: return
        
        h = POS(x)
        a_k = {}
        for k in xtween(i, j):
            p_out = f(i,j, x,loc_h, sent)
            if not p_out > 0.0:
                continue
            p_L =   e(i,k, x,loc_h, sent)
            if not p_L > 0.0:
                continue

            for loc_a,a in locs(sent_nums, k,j): # k<=loc_l(a)<j 
                p_rule = g.p_GO_AT_or0(a, h, RIGHT, adj(k, loc_h))
                p_R = e(k,j, (SEAL,a),loc_a, sent)
                p = p_L * p_out * p_R * p_rule 
                try:    a_k[a] += p
                except KeyError: a_k[a]  = p

        for a,p in a_k.iteritems():
            try:    fr['hat_a','num',a,x] += p / p_sent
            except KeyError: fr['hat_a','num',a,x]  = p / p_sent
    # end reest_freq.w_right()

    # in reest_freq:    
    for sent in corpus:
        if 'REEST' in DEBUG:
            print sent
        ichart = {}
        ochart = {}
        p_sent = inner_sent(g, sent, ichart)
        fr['ROOT','den'] += 1 # divide by p_sent per h!
        
        sent_nums = g.sent_nums(sent)
        
        for loc_h,h in locs(sent_nums,0,len(sent)+1): # locs-stop is exclusive, thus +1
            # root:
            fr['ROOT','num',h] += g.p_ROOT[h] * e(0,len(sent), (SEAL,h),loc_h, sent) \
                / p_sent
            
            loc_l_h = loc_h
            loc_r_h = loc_l_h+1 
            
            # left non-adjacent stop:
            for i in xlt(loc_l_h):
                fr['STOP','num',(GOL,h),NON] += c(loc_l_h, j, (LGOR, h),loc_h, sent)
                fr['STOP','den',(GOL,h),NON] += c(loc_l_h, j, (GOL, h),loc_h, sent)
                for j in xgteq(loc_r_h, sent):
                    fr['STOP','num',(RGOL,h),NON] += c(i, j, (SEAL, h),loc_h, sent)
                    fr['STOP','den',(RGOL,h),NON] += c(i, j, (RGOL, h),loc_h, sent)
            # left adjacent stop, i = loc_l_h
            fr['STOP','num',(GOL,h),ADJ] += c(loc_l_h, loc_r_h, (LGOR, h),loc_h, sent)
            fr['STOP','den',(GOL,h),ADJ] += c(loc_l_h, loc_r_h, (GOL, h),loc_h, sent)
            for j in xgteq(loc_r_h, sent): 
                fr['STOP','num',(RGOL,h),ADJ] += c(loc_l_h, j, (SEAL, h),loc_h, sent)
                fr['STOP','den',(RGOL,h),ADJ] += c(loc_l_h, j, (RGOL, h),loc_h, sent)
            # right non-adjacent stop:
            for j in xgt(loc_r_h, sent):
                fr['STOP','num',(GOR,h),NON] += c(loc_l_h, j, (RGOL, h),loc_h, sent)
                fr['STOP','den',(GOR,h),NON] += c(loc_l_h, j, (GOR,  h),loc_h, sent)
                for i in xlteq(loc_l_h):
                    fr['STOP','num',(LGOR,h),NON] += c(loc_l_h, j, (SEAL, h),loc_h, sent)
                    fr['STOP','den',(LGOR,h),NON] += c(loc_l_h, j, (LGOR,  h),loc_h, sent)
            # right adjacent stop, j = loc_r_h
            fr['STOP','num',(GOR,h),ADJ] += c(loc_l_h, loc_r_h, (RGOL, h),loc_h, sent)
            fr['STOP','den',(GOR,h),ADJ] += c(loc_l_h, loc_r_h, (GOR,  h),loc_h, sent)
            for i in xlteq(loc_l_h):
                fr['STOP','num',(LGOR,h),ADJ] += c(loc_l_h, j, (SEAL, h),loc_h, sent)
                fr['STOP','den',(LGOR,h),ADJ] += c(loc_l_h, j, (LGOR,  h),loc_h, sent)

            # left attachment:
            if 'REEST_ATTACH' in DEBUG: 
                print "Lattach %s: for i < %s"%(g.numtag(h),sent[0:loc_h+1]) 
            for s_h in [RGOL, GOL]:
                x = (s_h, h)
                for i in xlt(loc_l_h):  # i < loc_l(h)
                    if 'REEST_ATTACH' in DEBUG: 
                        print "\tfor j >= %s"%sent[loc_h:len(sent)]
                    for j in xgteq(loc_r_h, sent): # j >= loc_r(h)
                        fr['hat_a','den',x] += c(i,j, x,loc_h, sent) # v_q in L&Y
                        if 'REEST_ATTACH' in DEBUG:
                            print "\t\tc( %d , %d, %s, %s, sent)=%.4f"%(i,j,node_str(x),loc_h,fr['hat_a','den',x])
                        w_left(i, j, x,loc_h, sent,sent_nums) # compute w for all a in sent

            # right attachment:
            if 'REEST_ATTACH' in DEBUG:
                print "Rattach %s: for i <= %s"%(g.numtag(h),sent[0:loc_h+1])
            for s_h in [GOR, LGOR]:
                x = (s_h, h)
                for i in xlteq(loc_l_h):  # i <= loc_l(h)
                    if 'REEST_ATTACH' in DEBUG:
                        print "\tfor j > %s"%sent[loc_h:len(sent)]
                    for j in xgt(loc_r_h, sent): # j > loc_r(h) 
                        fr['hat_a','den',x] += c(i,j, x,loc_h, sent) # v_q in L&Y
                        if 'REEST_ATTACH' in DEBUG:
                            print "\t\tc( %d , %d, %s, %s, sent)=%.4f"%(loc_h,j,node_str(x),loc_h,fr['hat_a','den',x])
                        w_right(i,j, x,loc_h, sent,sent_nums) # compute w for all a in sent
        # end for loc_h,h
    # end for sent
    
    return fr

def reestimate(old_g, corpus):
    fr = reest_freq(old_g, corpus)
    p_ROOT, p_STOP, p_ATTACH = {},{},{}

    for h in old_g.headnums():
        # reest_head changes p_ROOT, p_STOP, p_ATTACH
        reest_head(h, fr, old_g, p_ROOT, p_STOP, p_ATTACH)
    p_ORDER = old_g.p_ORDER
    numtag, tagnum = old_g.get_nums_tags()
    
    new_g = DMV_Grammar(numtag, tagnum, p_ROOT, p_STOP, p_ATTACH, p_ORDER)
    return new_g


def reest_head(h, fr, g, p_ROOT, p_STOP, p_ATTACH):
    "Given a single head, update g with the reestimated probability."
    # remove 0-prob stuff? todo
    try:
        p_ROOT[h] = fr['ROOT','num',h] / fr['ROOT','den']
    except KeyError:
        p_ROOT[h] = 0.0
    
    for dir in [LEFT,RIGHT]:
        for adj in [ADJ, NON]: # p_STOP
            p_STOP[h, dir, adj] = 0.0
            for s_h in dirseal(dir):
                x = (s_h,h)
                p = fr['STOP','den', x, adj]
                if p > 0.0:
                    p = fr['STOP', 'num', x, adj] / p
                p_STOP[h, dir, adj] += p

        for s_h in dirseal(dir): # make hat_a for p_ATTACH
            x = (s_h,h)
            p_c = fr['hat_a','den',x]
            
            for a in g.headnums():
                if (a,h,dir) not in p_ATTACH:
                    p_ATTACH[a,h,dir] = 0.0
                try: # (a,x) might not be in hat_a
                    p_ATTACH[a,h,dir] += fr['hat_a','num',a,x] / p_c 
                except KeyError: pass
                except ZeroDivisionError: pass



        
        
###################################################
#              Reestimation v.2:                  #
#            Heads as outer loop                  #
###################################################

def locs_h(h, sent_nums):
    '''Return the between-word locations of all tokens of h in sent.'''
    return [loc_w for loc_w,w in locs(sent_nums, 0, len(sent_nums))
            if w == h]

def locs_a(a, sent_nums, start, stop):
    '''Return the between-word locations of all tokens of h in some
    fragment of sent. We make sure to offset the locations correctly
    so that for any w in the returned list, sent[w]==loc_w.
    
    start is inclusive, stop is exclusive, as in klein-thesis and
    Python's list-slicing (eg. return left-loc).'''
    return [loc_w for loc_w,w in locs(sent_nums, start, stop)
            if w == a]

def inner2(i, j, node, loc_h, g, sent):
    ichart,ochart = g.get_iochart(s_n)
    try: p = ichart[i,j,x,loc_h]
    except KeyError: p = inner(i,j,x,loc_h,g,sent,ichart) 
    g.set_iochart(s_n,ichart,ochart)
    return p

def inner_sent2(g, sent):
    ichart,ochart = g.get_iochart(s_n)
    p = inner_sent(g,sent,ichart)
    g.set_iochart(s_n,ichart,ochart)
    return p

def outer2(i, j,w_node,loc_w, g, sent):
    ichart,ochart = g.get_iochart(s_n)
    try: p = ochart[i,j,w_node,loc_w]
    except KeyError: p = inner(i,j,w_node,loc_w,g,sent,ichart,ochart) 
    g.set_iochart(s_n,ichart,ochart)
    return p

def reestimate2(old_g, corpus):
    p_ROOT, p_STOP, p_ATTACH = {},{},{}
    
    for h in old_g.headnums():
        # reest_head changes p_ROOT, p_STOP, p_ATTACH
        reest_head2(h, old_g, corpus, p_ROOT, p_STOP, p_ATTACH)
    p_ORDER = old_g.p_ORDER
    numtag, tagnum = old_g.get_nums_tags()
    
    new_g = DMV_Grammar(numtag, tagnum, p_ROOT, p_STOP, p_ATTACH, p_ORDER)
    return new_g

def hat_d2(xbar, x, xi, xj, g, corpus): # stop helper
    def c(x,loc_x,i,j): return c2(x,loc_x,i,j,g,s_n,sent)
    
    h = POS(x)
    if h != POS(xbar): raise ValueError
    
    num, den = 0.0, 0.0
    for s_n,sent in [(g.sent_nums(sent),sent) for sent in corpus]:
        for loc_h in locs_h(h,s_n):
            loc_l_h, loc_r_h = loc_h, loc_h + 1
            for i in xi(loc_l_h):
                for j in xj(loc_r_h, s_n):
#                     print "s:%s %d,%d"%(sent,i,j)
                    num += c(xbar,loc_h,i,j) 
                    den += c(x,loc_h,i,j)    
    if den == 0.0:
        return den                
    return num/den # eg. SEAL/RGOL, xbar/x 
    

def c2(x,loc_h,i,j,g,s_n,sent):
    ichart,ochart = g.get_iochart(s_n)
    
    def f(i,j,x,loc_h): # P_{OUTSIDE}
        try: return ochart[i,j,x,loc_h]
        except KeyError: return outer(i,j,x,loc_h,g,sent,ichart,ochart)
    def e(i,j,x,loc_h): # P_{INSIDE}
        try: return ichart[i,j,x,loc_h]
        except KeyError: return inner(i,j,x,loc_h,g,sent,ichart) 

    p_sent = inner_sent(g, sent, ichart)
    if not p_sent > 0.0:
        return p_sent
    
    p_in = e(i,j, x,loc_h)
    if not p_in > 0.0: 
        return p_in
    
    p_out = f(i,j, x,loc_h)

    g.set_iochart(s_n,ichart,ochart)
    return p_in * p_out / p_sent

def w2(a, x,loc_h, dir, i, j, g, s_n,sent):
    ichart,ochart = g.get_iochart(s_n)
    
    def f(i,j,x,loc_h): # P_{OUTSIDE}
        try: return ochart[i,j,x,loc_h]
        except KeyError: return outer(i,j,x,loc_h,g,sent,ichart,ochart)
    def e(i,j,x,loc_h): # P_{INSIDE}
        try: return ichart[i,j,x,loc_h]
        except KeyError: return inner(i,j,x,loc_h,g,sent,ichart) 

    h = POS(x)
    p_sent = inner_sent(g, sent, ichart)
    
    if dir == LEFT:
        L, R = (SEAL,a),x
    else:
        L, R = x,(SEAL,a)
    w_sum = 0.0
    
    for k in xtween(i,j):
        if dir == LEFT:
            start, stop = i, k
        else:
            start, stop = k, j
        for loc_a in locs_a(a, s_n, start, stop):
            if dir == LEFT:
                loc_L, loc_R = loc_a, loc_h
            else:
                loc_L, loc_R = loc_h, loc_a
            p = g.p_GO_AT_or0(a,h,dir,adj(k,loc_h))
            in_L = e(i,k,L,loc_L) 
            in_R = e(k,j,R,loc_R) 
            out =  f(i,j,x,loc_h)
            w_sum += p * in_L * in_R * out
            
    g.set_iochart(s_n,ichart,ochart)
    return w_sum/p_sent

def hat_a2(a, x, dir, g, corpus): # attachment helper
    def w(a,x,loc_x,dir,i,j): return w2(a,x,loc_x,dir,i,j,g,s_n,sent)
    def c(x,loc_x,i,j): return c2(x,loc_x,i,j,g,s_n,sent)

    h = POS(x)
    if dir == LEFT:
        xi, xj = xlt, xgteq
    else:
        xi, xj = xlteq, xgt 
    den, num = 0.0, 0.0
    
    for s_n,sent in [(g.sent_nums(sent),sent) for sent in corpus]:
        for loc_h in locs_h(h,s_n):
            loc_l_h, loc_r_h = loc_h, loc_h + 1
            for i in xi(loc_l_h):
                for j in xj(loc_r_h,sent):
                    num += w(a, x,loc_h, dir, i,j)
                    den += c(x,loc_h, i,j)
    if den == 0.0:
        return den
    return num/den

def reest_root2(h,g,corpus):
    sum = 0.0
    corpus_size = 0.0
    for s_n,sent in [(g.sent_nums(sent),sent) for sent in corpus]:
        num, den = 0.0, 0.0
        corpus_size += 1.0
        ichart, ochart = g.get_iochart(s_n)
        den += inner_sent(g, sent, ichart)
        for loc_h in locs_h(h,s_n):
            num += \
                g.p_ROOT[h] * \
                inner(0, len(s_n), (SEAL,h), loc_h, g, sent, ichart) 
        g.set_iochart(s_n, ichart, ochart)
        sum += num / den
    return sum / corpus_size

def reest_head2(h, g, corpus, p_ROOT, p_STOP, p_ATTACH):
    print "h: %d=%s ..."%(h,g.numtag(h)),
    def hat_d(xbar,x,xi,xj): return hat_d2(xbar,x,xi,xj, g, corpus)
    def hat_a(a, x, dir  ): return hat_a2(a, x, dir,   g, corpus)
    
    p_STOP[h, LEFT,NON] = \
        hat_d((SEAL,h),(RGOL,h),xlt,  xgteq) + \
        hat_d((LGOR,h),( GOL,h),xlt,  xeq) 
    p_STOP[h, LEFT,ADJ] = \
        hat_d((SEAL,h),(RGOL,h),xeq,  xgteq) + \
        hat_d((LGOR,h),( GOL,h),xeq,  xeq)
    p_STOP[h,RIGHT,NON] = \
        hat_d((RGOL,h),( GOR,h),xeq,  xgt) + \
        hat_d((SEAL,h),(LGOR,h),xlteq,xgt)
    p_STOP[h,RIGHT,ADJ] = \
        hat_d((RGOL,h),( GOR,h),xeq,  xeq) + \
        hat_d((SEAL,h),(LGOR,h),xlteq,xeq)
    print "stops done...",

    p_ROOT[h] = reest_root2(h,g,corpus)
    print "root done...",
    
    for a in g.headnums():
        p_ATTACH[a,h,LEFT] = \
            hat_a(a, (GOL,h),LEFT) + \
            hat_a(a,(RGOL,h),LEFT)
        p_ATTACH[a,h,RIGHT] = \
            hat_a(a, (GOR,h),RIGHT) + \
            hat_a(a,(LGOR,h),RIGHT)

    print "attachment done"
        


###################################################
#             Most Probable Parse:                #
###################################################

STOPKEY = (-1,-1,STOP,-1)
ROOTKEY = (-1,-1,ROOT,-1)

def make_mpptree(g, sent):
    '''Tell inner() to make an mpptree, connect ROOT to this. (Logically,
    this should be part of inner_sent though...)'''
    ichart = {}
    mpptree = { ROOTKEY:(0.0, ROOTKEY, None) }
    for loc_w,w in locs(g.sent_nums(sent),0,len(sent)):
        p = g.p_ROOT[w] * inner(0, len(sent), (SEAL,w), loc_w, g, sent, ichart, mpptree)
        L = ROOTKEY
        R = (0,len(sent), (SEAL,w), loc_w)
        if mpptree[ROOTKEY][0] < p:
            mpptree[ROOTKEY] = (p, L, R)
    return mpptree
        
def parse_mpptree(mpptree, sent):
    '''mpptree is a dict of the form {k:(p,L,R),...}; where k, L and R
    are `keys' of the form (i,j,node,loc).

    returns an mpp of the form [((head, loc_h),(arg, loc_a)), ...],
    where head and arg are tags.'''
    # local functions for clear access to mpptree:
    def k_node(key):
        return key[2]
    def k_POS(key):
        return POS(k_node(key))
    def k_seals(key):
        return seals(k_node(key))
    def k_locnode(key):
        return (k_node(key),key[3])
    def k_locPOS(key):
        return (k_POS(key),key[3])
    def k_terminal(key):
        s_k = k_seals(key) # i+1 == j 
        return key[0] + 1 == key[1] and (s_k == GOR or s_k == GOL)
    def t_L(tree_entry):
        return tree_entry[1]
    def t_R(tree_entry):
        return tree_entry[2]
        
    # arbitrarily, "ROOT attaches to right". We add it here to
    # avoid further complications:
    firstkey = t_R(mpptree[ROOTKEY])
    deps = set([ (k_locPOS(ROOTKEY), k_locPOS(firstkey), RIGHT) ])
    q = [firstkey]

    while len(q) > 0:
        k = q.pop()
        if k_terminal(k):
            continue
        else:
            L = t_L( mpptree[k] )
            R = t_R( mpptree[k] )
            if k_locnode( k ) == k_locnode( L ): # Rattach
                deps.add((k_locPOS( k ), k_locPOS( R ), LEFT))
                q.extend( [L, R] )
            elif k_locnode( k ) == k_locnode( R ): # Lattach
                deps.add((k_locPOS( k ), k_locPOS( L ), RIGHT))
                q.extend( [L, R] )
            elif R == STOPKEY:
                q.append( L )
            elif L == STOPKEY:
                q.append( R )
    return deps

def mpp(g, sent):
    tagf = g.numtag # localized function, todo: speed-test
    mpptree = make_mpptree(g, sent)
    return set([((tagf(h), loc_h), (tagf(a), loc_a))
                for (h, loc_h),(a,loc_a),dir in parse_mpptree(mpptree,sent)])


########################################################################
#     testing functions:                                               #
########################################################################

testcorpus = [s.split() for s in ['det nn vbd c vbd','vbd nn c vbd',
                                  'det nn vbd',      'det nn vbd c pp', 
                                  'det nn vbd',      'det vbd vbd c pp', 
                                  'det nn vbd',      'det nn vbd c vbd', 
                                  'det nn vbd',      'det nn vbd c vbd', 
                                  'det nn vbd',      'det nn vbd c vbd', 
                                  'det nn vbd',      'det nn vbd c pp', 
                                  'det nn vbd pp',   'det nn vbd', ]]

def testgrammar():
    import loc_h_harmonic
    reload(loc_h_harmonic)

    # make sure these are the way they were when setting up the tests:
    loc_h_harmonic.HARMONIC_C = 0.0
    loc_h_harmonic.FNONSTOP_MIN = 25
    loc_h_harmonic.FSTOP_MIN = 5
    loc_h_harmonic.RIGHT_FIRST = 1.0 
    loc_h_harmonic.OLD_STOP_CALC = True
    
    return loc_h_harmonic.initialize(testcorpus)

def testreestimation2():
    g2 = testgrammar()
    reestimate2(g2, testcorpus)
    return g2

def testreestimation():
    g = testgrammar()
    g = reestimate(g, testcorpus)
    return g


def testmpp_regression(mpptree,k_n):
    mpp = {ROOTKEY: (2.877072116829971e-05, STOPKEY, (0, 3, (2, 3), 1)),
 (0, 1, (1, 1), 0): (0.1111111111111111, (0, 1, (0, 1), 0), STOPKEY),
 (0, 1, (2, 1), 0): (0.049382716049382713, STOPKEY, (0, 1, (1, 1), 0)),
 (0, 3, (1, 3), 1): (0.00027619892321567721,
                       (0, 1, (2, 1), 0),
                       (1, 3, (1, 3), 1)),
 (0, 3, (2, 3), 1): (0.00012275507698474543, STOPKEY, (0, 3, (1, 3), 1)),
 (1, 3, (0, 3), 1): (0.025280986819448362,
                       (1, 2, (0, 3), 1),
                       (2, 3, (2, 4), 2)),
 (1, 3, (1, 3), 1): (0.0067415964851862296, (1, 3, (0, 3), 1), STOPKEY),
 (2, 3, (1, 4), 2): (0.32692307692307693, (2, 3, (0, 4), 2), STOPKEY),
 (2, 3, (2, 4), 2): (0.037721893491124266, STOPKEY, (2, 3, (1, 4), 2))}
    for k,(v,L,R) in mpp.iteritems():
        k2 = k[0:k_n] # 3 if the new does not check loc_h
        if type(k)==str:
            k2 = k
        if k2 not in mpptree:
            print "mpp regression, %s missing"%(k2,)
        else:
            vnew = mpptree[k2][0]
            if not "%.10f"%vnew == "%.10f"%v:
                print "mpp regression, wanted %s=%.5f, got %.5f"%(k2,v,vnew)
    

def testgrammar_a():
    h, a = 0, 1
    p_ROOT, p_STOP, p_ATTACH, p_ORDER = {},{},{},{}
    p_ROOT[h] = 0.9
    p_ROOT[a] = 0.1
    p_STOP[h,LEFT,NON] = 1.0
    p_STOP[h,LEFT,ADJ] = 1.0
    p_STOP[h,RIGHT,NON] = 0.4 # RSTOP
    p_STOP[h,RIGHT,ADJ] = 0.3 # RSTOP
    p_STOP[a,LEFT,NON] = 1.0
    p_STOP[a,LEFT,ADJ] = 1.0
    p_STOP[a,RIGHT,NON] = 0.4 # RSTOP
    p_STOP[a,RIGHT,ADJ] = 0.3 # RSTOP
    p_ATTACH[a,h,LEFT] = 1.0  # not used
    p_ATTACH[a,h,RIGHT] = 1.0 # not used
    p_ATTACH[h,a,LEFT] = 1.0  # not used
    p_ATTACH[h,a,RIGHT] = 1.0 # not used
    p_ATTACH[h,h,LEFT] = 1.0  # not used
    p_ATTACH[h,h,RIGHT] = 1.0 # not used
    p_ORDER[(GOR, h)] = 1.0
    p_ORDER[(GOL, h)] = 0.0
    p_ORDER[(GOR, a)] = 1.0
    p_ORDER[(GOL, a)] = 0.0
    g = DMV_Grammar({h:'h',a:'a'}, {'h':h,'a':a}, p_ROOT, p_STOP, p_ATTACH, p_ORDER)
    # these probabilities are impossible so add them manually:
    g.p_GO_AT[a,a,LEFT,NON] = 0.4  # Lattach
    g.p_GO_AT[a,a,LEFT,ADJ] = 0.6  # Lattach
    g.p_GO_AT[h,a,LEFT,NON] = 0.2  # Lattach to h
    g.p_GO_AT[h,a,LEFT,ADJ] = 0.1  # Lattach to h
    g.p_GO_AT[a,a,RIGHT,NON] = 1.0 # Rattach 
    g.p_GO_AT[a,a,RIGHT,ADJ] = 1.0 # Rattach 
    g.p_GO_AT[h,a,RIGHT,NON] = 1.0 # Rattach to h
    g.p_GO_AT[h,a,RIGHT,ADJ] = 1.0 # Rattach to h
    g.p_GO_AT[h,h,LEFT,NON] = 0.2 # Lattach
    g.p_GO_AT[h,h,LEFT,ADJ] = 0.1 # Lattach
    g.p_GO_AT[a,h,LEFT,NON] = 0.4 # Lattach to a
    g.p_GO_AT[a,h,LEFT,ADJ] = 0.6 # Lattach to a
    g.p_GO_AT[h,h,RIGHT,NON] = 1.0 # Rattach
    g.p_GO_AT[h,h,RIGHT,ADJ] = 1.0 # Rattach 
    g.p_GO_AT[a,h,RIGHT,NON] = 1.0 # Rattach to a
    g.p_GO_AT[a,h,RIGHT,ADJ] = 1.0 # Rattach to a
    return g
    

def testgrammar_h():                         
    h = 0
    p_ROOT, p_STOP, p_ATTACH, p_ORDER = {},{},{},{}
    p_ROOT[h] = 1.0
    p_STOP[h,LEFT,NON] = 1.0
    p_STOP[h,LEFT,ADJ] = 1.0
    p_STOP[h,RIGHT,NON] = 0.4
    p_STOP[h,RIGHT,ADJ] = 0.3
    p_ATTACH[h,h,LEFT] = 1.0  # not used
    p_ATTACH[h,h,RIGHT] = 1.0 # not used
    p_ORDER[(GOR, h)] = 1.0
    p_ORDER[(GOL, h)] = 0.0
    g = DMV_Grammar({h:'h'}, {'h':h}, p_ROOT, p_STOP, p_ATTACH, p_ORDER)
    g.p_GO_AT[h,h,LEFT,NON] = 0.6 # these probabilities are impossible
    g.p_GO_AT[h,h,LEFT,ADJ] = 0.7 # so add them manually...
    g.p_GO_AT[h,h,RIGHT,NON] = 1.0
    g.p_GO_AT[h,h,RIGHT,ADJ] = 1.0
    return g



def testreestimation_h():
    DEBUG.add('REEST')
    g = testgrammar_h()
    reestimate(g,['h h h'.split()])


def test(wanted, got):
    if not wanted == got:
        raise Warning, "Regression! Should be %s: %s" % (wanted, got)
            
def regression_tests():
    testmpp_regression(make_mpptree(testgrammar(), testcorpus[2]),4)
    h = 0

    test("0.120",
         "%.3f" % inner(0, 2, (SEAL,h), 0, testgrammar_h(), 'h h'.split(),{}))
    test("0.063",
         "%.3f" % inner(0, 2, (SEAL,h), 1, testgrammar_h(), 'h h'.split(),{}))
    test("0.1842",
         "%.4f" % inner_sent(testgrammar_h(), 'h h h'.split(),{}))
    
    test("0.1092",
         "%.4f" % inner(0, 3, (SEAL,0), 0, testgrammar_h(), 'h h h'.split(),{}))
    test("0.0252",
         "%.4f" % inner(0, 3, (SEAL,0), 1, testgrammar_h(), 'h h h'.split(),{}))
    test("0.0498",
         "%.4f" % inner(0, 3, (SEAL,h), 2, testgrammar_h(), 'h h h'.split(),{}))
    
    test("0.58" ,
         "%.2f" % outer(1, 3, (RGOL,h), 2, testgrammar_h(),'h h h'.split(),{},{}))
    test("0.61" , # ftw? can't be right... there's an 0.4 shared between these two...
         "%.2f" % outer(1, 3, (RGOL,h), 1, testgrammar_h(),'h h h'.split(),{},{}))
    
    test("0.00" ,
         "%.2f" % outer(1, 3, (RGOL,h), 0, testgrammar_h(),'h h h'.split(),{},{}))
    test("0.00" ,
         "%.2f" % outer(1, 3, (RGOL,h), 3, testgrammar_h(),'h h h'.split(),{},{}))
    
    test("0.1089" ,
         "%.4f" % outer(0, 1, (GOR,h),  0,testgrammar_a(),'h a'.split(),{},{}))
    test("0.3600" ,
         "%.4f" % outer(0, 2, (GOR,h),  0,testgrammar_a(),'h a'.split(),{},{}))
    test("0.0000" ,
         "%.4f" % outer(0, 3, (GOR,h),  0,testgrammar_a(),'h a'.split(),{},{}))

    # todo: add more of these tests...
    


def compare_grammars(g1,g2):
    result = ""
    for d1,d2 in [(g1.p_ATTACH,g2.p_ATTACH),(g1.p_STOP,g2.p_STOP),
                  (g1.p_ORDER, g2.p_ORDER), (g1.p_ROOT,g2.p_ROOT) ]:
        for k,v in d1.iteritems():
            if k not in d2:
                result += "\nreestimate1[%s]=%s missing from reestimate2"%(k,v)
            elif "%s"%d2[k] != "%s"%v:
                result += "\nreestimate1[%s]=%s while \nreestimate2[%s]=%s."%(k,v,k,d2[k])
        for k,v in d2.iteritems():
            if k not in d1:
                result += "\nreestimate2[%s]=%s missing from reestimate1"%(k,v)
    return result


def testNVNgrammar():
    import loc_h_harmonic

    # make sure these are the way they were when setting up the tests:
    loc_h_harmonic.HARMONIC_C = 0.0
    loc_h_harmonic.FNONSTOP_MIN = 25
    loc_h_harmonic.FSTOP_MIN = 5
    loc_h_harmonic.RIGHT_FIRST = 1.0 
    loc_h_harmonic.OLD_STOP_CALC = True
    
    g = loc_h_harmonic.initialize(['n v n'.split()])
    return g # todo

def testIO():
    g = testgrammar()
    inners = [(sent, inner_sent(g, sent, {})) for sent in testcorpus]
    return inners

if __name__ == "__main__":
    DEBUG.clear()
    regression_tests()

#    import profile
#    profile.run('testreestimation()')

#     import timeit
#     print timeit.Timer("loc_h_dmv.testreestimation()",'''import loc_h_dmv
# reload(loc_h_dmv)''').timeit(1)


#     print "mpp-test:"
#     import pprint
#     for s in testcorpus:
#         print "sent:%s\nparse:set(\n%s)"%(s,pprint.pformat(list(mpp(testgrammar(), s)),
#                                                     width=40))

#     g1 = testreestimation()
#     g2 = testreestimation2()
#     print compare_grammars(g1,g2)







if False:
    g = testNVNgrammar()
    q_sent = inner_sent(g,'n v n'.split(),{})
    q_tree = {}
    q_tree[1] = 2.7213e-06 # n_0 -> v, n_0 -> n_2
    q_tree[2] = 9.738e-06   # n -> v -> n
    q_tree[3] = 2.268e-06   # n_0 -> n_2 -> v
    q_tree[4] = 2.7213e-06 # same as 1-3
    q_tree[5] = 9.738e-06
    q_tree[6] = 2.268e-06
    q_tree[7] = 1.086e-05   # n <- v -> n (e-05!!!)
    f_T_q = {}
    for i,q_t in q_tree.iteritems():
        f_T_q[i] = q_t / q_sent
    import pprint
    pprint.pprint(q_tree)
    pprint.pprint(f_T_q)
    print sum([f for f in f_T_q.values()])
    
    def treediv(num,den):
        return \
            sum([f_T_q[i] for i in num ]) / \
            sum([f_T_q[i] for i in den ])
    g2 = {}
#     g2['root --> _n_'] = treediv( (1,2,3,4,5,6), (1,2,3,4,5,6,7) )
#     g2['root --> _v_'] = treediv( (7,), (1,2,3,4,5,6,7) )
#     g2['_n_ --> STOP n><'] = treediv( (1,2,3,4,5,6,7,1,2,3,4,5,6,7),
#                                       (1,2,3,4,5,6,7,1,2,3,4,5,6,7))

#     g2['_n_ --> STOP n>< NON'] = treediv( (3,4,5,6),
#                                           (3,4,5,6,4) )

#     g2['_v_ --> STOP v><'] = treediv( (1,2,3,4,5,6,7),
#                                       (1,2,3,4,5,6,7) )
#     nlrtrees = (1,2,3,4,5,6,7,1,2,3,4,5,6,7,
#                 3,4,4,5,6)
#     g2['n>< --> _n_ n><'] = treediv( (  4,  6), nlrtrees )
#     g2['n>< --> _v_ n><'] = treediv( (3,4,5), nlrtrees )
#     g2['n>< --> n> STOP'] = treediv( (1,2,3,4,5,6,7,1,2,3,4,5,6,7),
#                                      nlrtrees )

#     g2['n>< --> n> STOP ADJ'] = treediv( (      4,5,  7,1,2,3,4,5,6,7),
#                                          nlrtrees )
#     g2['n>< --> n> STOP NON'] = treediv( (1,2,3,    6),
#                                          nlrtrees )

#     vlrtrees = (1,2,3,4,5,6,7,
#                 7,5)
#     g2['v>< --> _n_ v><'] = treediv( (5,7), vlrtrees )
#     g2['v>< --> v> STOP'] = treediv( (1,2,3,4,5,6,7), vlrtrees )
#     nrtrees = (1,2,3,4,5,6,7,1,2,3,4,5,6,7,
#                1,1,2,3,6)
#     g2['n> --> n> _n_'] = treediv( (1,3), nrtrees )
#     g2['n> --> n> _v_'] = treediv( (1,2,6), nrtrees )

#     g2['n> --> n> _n_ NON'] = treediv( (1,), nrtrees )
#     g2['n> --> n> _n_ ADJ'] = treediv( (      3,), nrtrees )
#     g2['n> --> n> _v_ ADJ'] = treediv( (  1,2,  6), nrtrees )

#     vrtrees = (1,2,3,4,5,6,7,
#                7,2)
#     g2['v> --> v> _n_'] = treediv( (2,7), vrtrees )

#     g2[' v|n,R '] = treediv( (1,  2,  6),
#                              (1,1,2,3,6) )
#     g2[' n|n,R '] = treediv( (1,    3),
#                              (1,1,2,3,6) )
    
    g2[' stop|n,R,non '] = treediv( (  1,2,3,6),
                                    (1,1,2,3,6) )
    g2[' v|n,left '] = treediv( (    3,4,5),
                                (6,4,3,4,5) )
    g2[' n|n,left '] = treediv( (6,4),
                                (6,4,3,4,5) )
    
    pprint.pprint(g2)
    g3 = reestimate2(g, ['n v n'.split()])
    print g3
    g4 = reestimate2(g, ['n v n'.split()])
    print g4

    
