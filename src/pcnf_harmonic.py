# pcnf_harmonic.py, initialization for pcnf_dmv.py

from pcnf_dmv import * # better way to do this?
from loc_h_harmonic import taglist, init_freq # neutral as regards cnf/loc_h

# todo: tweak this
HARMONIC_C = 0.0
FNONSTOP_MIN = 25
FSTOP_MIN = 5

##############################
#      Initialization        #
##############################
def init_normalize(f, tags, tagnum, numtag):
    '''Use frequencies (and sums) in f to return create p_STOP and
    p_ATTACH; at the same time adding the context-free rules to the
    grammar using these probabilities. 

    Return a usable grammar.'''
    p_rules = []
    p_STOP, p_ROOT, p_ATTACH, p_terminals = {},{},{},{}
    for h, head in numtag.iteritems():
        p_ROOT[h] = float(f['ROOT', head]) / f['sum', 'ROOT']
        
        # p_STOP = STOP / (STOP + NOT_STOP)
        for dir in [LEFT,RIGHT]:
            for adj in [NON,ADJ]:
                p_STOP[h, dir, adj] = \
                    float(f[head, 'STOP', dir, adj]) / \
                    (f[head, 'STOP', dir, adj] + f[head, '-STOP', dir, adj])

        p_terminals[(GOR, h), head] = 1.0
        
        for dir in [LEFT, RIGHT]:
            for arg, val in f[head, dir].iteritems():
                p_ATTACH[tagnum[arg], h, dir] = float(val) / f[head,'sum',dir]
    
    return PCNF_DMV_Grammar(numtag, tagnum, p_ROOT, p_STOP, p_ATTACH, p_terminals)


def initialize(corpus):
    '''Return an initialized PCNF_DMV_Grammar
    corpus is a list of lists of tags.'''
    tags = taglist(corpus)
    tagnum, numtag = {}, {}
    for num, tag in enumerate(tags):
        tagnum[tag] = num
        numtag[num] = tag
    # f: frequency counts used in initialization, mostly distances
    f = init_freq(corpus, tags)
    g = init_normalize(f, tags, tagnum, numtag)
    return g


if __name__ == "__main__":
    print "--------initialization testing------------"
    print initialize([['foo', 'two','foo','foo'],
                      ['zero', 'one','two','three']])
    
    for (n,s) in [(95,5),(5,5)]:
        FNONSTOP_MIN = n
        FSTOP_MIN = s
        
        testcorpus = [s.split() for s in ['det nn vbd c nn vbd nn','det nn vbd c nn vbd pp nn',
                                          'det nn vbd nn','det nn vbd c nn vbd pp nn', 
                                          'det nn vbd nn','det nn vbd c nn vbd pp nn', 
                                          'det nn vbd nn','det nn vbd c nn vbd pp nn', 
                                          'det nn vbd nn','det nn vbd c nn vbd pp nn', 
                                          'det nn vbd pp nn','det nn vbd det nn', ]]
        g = initialize(testcorpus)

    

