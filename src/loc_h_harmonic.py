# loc_h_harmonic.py, initialization for loc_h_dmv.py

from loc_h_dmv import * # better way to do this?

# todo: tweak these
HARMONIC_C = 0.0 # C_A in report.pdf
STOP_C = 1.0     # C_S in report.pdf
NSTOP_C = 1.0    # C_N in report.pdf
FSTOP_MIN = 1.0  # C_M in report.pdf

FNONSTOP_MIN = 0.0 # for OLD_STOP_CALC. 0.0 on FSTOP_MIN gives many
                   # zero-probabilities for OLD_STOP_CALC
OLD_STOP_CALC = True

RIGHT_FIRST = 1.0 # apparently right-first is best for DMV-only 

##############################
#      Initialization        #
##############################
def taglist(corpus):
    '''sents is of this form:
[['tag', ...], ['tag2', ...], ...]

Return a list of the tags. (Has to be ordered for enumerating to be
consistent.)

Fortunately only has to run once.
'''
    tagset = set()
    for sent in corpus:
        for tag in sent:
            tagset.add(tag)
    if 'ROOT' in tagset:
        raise ValueError("it seems we must have a new ROOT symbol")
    return list(tagset)





def init_zeros(tags):
    '''Return a frequency dictionary with DMV-relevant keys set to 0 or
    {}.

    Todo: tweak (especially for f_STOP).'''
    f = {} 
    for tag in tags:
        f['ROOT', tag] = 0
        f['sum', 'ROOT'] = 0
        for dir in [LEFT, RIGHT]:
            for adj in [ADJ, NON]:
                f[tag, 'STOP', dir, adj] = 0.0
                if OLD_STOP_CALC:
                    f[tag, 'STOP', dir, adj] = FSTOP_MIN
                    f[tag, '-STOP', dir, adj] = FNONSTOP_MIN
        f[tag, RIGHT] = {}
        f[tag, LEFT] = {}
        f[tag, 'sum', RIGHT] = 0.0
        f[tag, 'sum', LEFT] = 0.0
    return f

def init_freq(corpus, tags):
    '''Returns f, a dictionary with these types of keys:
    - ('ROOT', tag) is basically just the frequency of tag
    - (tag, 'STOP', 'LN') is for P_STOP(STOP|tag, left, non_adj);
      etc. for 'RN', 'LA', 'LN', '-STOP'.
    - (tag, LEFT) is a dictionary of arg:f, where head could take arg
      to direction LEFT (etc. for RIGHT) and f is "harmonically" divided
      by distance, used for finding P_CHOOSE

      Does this stuff:
      1. counts word frequencies for f_ROOT
      2. adds to certain f_STOP counters if a word is found first,
         last, first or second, or last or second to last in the sentence
         (Left Adjacent, Left Non-Adjacent, etc)
      3. adds to f_CHOOSE(arg|head) a "harmonic" number (divided by
         distance between arg and head)
    '''
    f = init_zeros(tags)
    
    for sent in corpus: # sent is ['VBD', 'NN', ...]
        n = len(sent) - 1
        # NOTE: head in DMV_Rule is a number, while this is the string
        for loc_h, head in enumerate(sent): 
            # todo grok: how is this different from just using straight head
            # frequency counts, for the ROOT probabilities?
            f['ROOT', head] += 1.0
            f['sum', 'ROOT'] += 1.0
            
            if OLD_STOP_CALC:
                f[head,  'STOP',  LEFT,NON] += (loc_h == 1)     # second word
                f[head, '-STOP',  LEFT,NON] += (not loc_h == 1) # not second
                f[head,  'STOP',  LEFT,ADJ] += (loc_h == 0)     # first word
                f[head, '-STOP',  LEFT,ADJ] += (not loc_h == 0) # not first
                f[head,  'STOP', RIGHT,NON] += (loc_h == n - 1)     # second-to-last
                f[head, '-STOP', RIGHT,NON] += (not loc_h == n - 1) # not second-to-last
                f[head,  'STOP', RIGHT,ADJ] += (loc_h == n)         # last word
                f[head, '-STOP', RIGHT,ADJ] += (not loc_h == n)     # not last
            else:
                f[head,  'STOP',  LEFT,NON] += (loc_h == 1)     # second word
                f[head,  'STOP',  LEFT,ADJ] += (loc_h == 0)     # first word
                f[head,  'STOP', RIGHT,NON] += (loc_h == n - 1) # second-to-last
                f[head,  'STOP', RIGHT,ADJ] += (loc_h == n)     # last word
#             elif OTHER_STOP_CALC:
#                 f[head,  'STOP',  LEFT,NON] += (loc_h == 1)     # second word
#                 f[head, '-STOP',  LEFT,NON] += (loc_h  > 1)     # after second
#                 f[head,  'STOP',  LEFT,ADJ] += (loc_h == 0)     # first word
#                 f[head, '-STOP',  LEFT,ADJ] += (loc_h  > 0)     # not first
#                 f[head,  'STOP', RIGHT,NON] += (loc_h == n - 1) # second-to-last
#                 f[head, '-STOP', RIGHT,NON] += (loc_h  < n - 1) # before second-to-last
#                 f[head,  'STOP', RIGHT,ADJ] += (loc_h == n)     # last word
#                 f[head, '-STOP', RIGHT,ADJ] += (loc_h  < n)     # not last
            
            # this is where we make the "harmonic" distribution. quite.
            for loc_a, arg in enumerate(sent):
                if loc_h != loc_a:
                    harmony = 1.0/abs(loc_h - loc_a) + HARMONIC_C
                    if loc_h > loc_a:
                        dir = LEFT
                    else:
                        dir = RIGHT
                    if arg not in f[head, dir]:
                        f[head, dir][arg] = 0.0
                    f[head, dir][arg] += harmony
                    f[head, 'sum', dir] += harmony
                # todo, optimization: possible to do both directions
                # at once here, and later on rule out the ones we've
                # done? does it actually speed things up?
    return f 

def init_normalize(f, tags, numtag, tagnum):
    '''Use frequencies (and sums) in f to return create p_STOP, p_ATTACH,
    p_ROOT.
    
    Return a usable DMV_Grammar.'''
    p_rules = []
    p_STOP, p_ROOT, p_ATTACH, p_ORDER = {},{},{},{}
    for h, head in numtag.iteritems():
        # f['ROOT', head] is just a simple frequency count
        p_ROOT[h] = f['ROOT', head] / f['sum', 'ROOT']
        
        for dir in [LEFT,RIGHT]:
            for adj in [NON,ADJ]:
                if OLD_STOP_CALC:
                    den = f[head, 'STOP', dir, adj] + f[head, '-STOP', dir, adj]
                    if den > 0.0:
                        p_STOP[h, dir, adj] = f[head, 'STOP', dir, adj] / den
                    else: p_STOP[h, dir, adj] = 1.0
                else: 
                    p_STOP[h, dir, adj] = \
                        (FSTOP_MIN + f[head, 'STOP', dir, adj] * STOP_C) / \
                        (FSTOP_MIN + f['ROOT',head] * (STOP_C + NSTOP_C))
                    
        p_ORDER[GOR, h] = RIGHT_FIRST
        p_ORDER[GOL, h] = 1.0 - RIGHT_FIRST

        for dir in [LEFT, RIGHT]:
            for arg, val in f[head, dir].iteritems():
                p_ATTACH[tagnum[arg], h, dir] = val / f[head,'sum',dir]

    return DMV_Grammar(numtag, tagnum, p_ROOT, p_STOP, p_ATTACH, p_ORDER)

def initialize(corpus):
    ''' corpus is a list of lists of tags.'''
    tags = taglist(corpus)
    numtag, tagnum = {}, {}
    for num, tag in enumerate(tags):
        tagnum[tag] = num
        numtag[num] = tag
    # f: frequency counts used in initialization, mostly distances
    f = init_freq(corpus, tags)
    g = init_normalize(f, tags, numtag, tagnum)

    g.HARMONIC_C = HARMONIC_C # for evaluations in main.py, todo: remove
    g.STOP_C = STOP_C
    g.NSTOP_C = NSTOP_C
    g.FNONSTOP_MIN = FNONSTOP_MIN
    g.FSTOP_MIN = FSTOP_MIN
    
    return g


def uniform_init(corpus):
    tags = taglist(corpus)
    p_ROOT,p_STOP,p_ATTACH,p_ORDER,numtag, tagnum = {},  {},  {},  {},  {}, {}
    ntags = len(tags)
    for num, tag in enumerate(tags):
        tagnum[tag] = num
        numtag[num] = tag
        p_ROOT[num] = 1.0 / ntags
        p_STOP[num,LEFT,NON] = 0.5
        p_STOP[num,LEFT,ADJ] = 0.5
        p_STOP[num,RIGHT,NON] = 0.5
        p_STOP[num,RIGHT,ADJ] = 0.5
        p_ORDER[GOR, num] = RIGHT_FIRST # NOT uniform...
        p_ORDER[GOL, num] = 1.0 - RIGHT_FIRST
        for num2, tag2 in enumerate(tags):
            p_ATTACH[num,num2,LEFT] = 1.0 / ntags
            p_ATTACH[num,num2,RIGHT] = 1.0 / ntags

    g = DMV_Grammar(numtag, tagnum, p_ROOT, p_STOP, p_ATTACH, p_ORDER)
    g.HARMONIC_C = 0.0 # for evaluations in main.py, todo: remove
    g.STOP_C = 0.0
    g.NSTOP_C = 0.0
    g.FNONSTOP_MIN = 0.0
    g.FSTOP_MIN = 0.0
    
    return g


##############################
#     testing functions:     #
##############################

if __name__ == "__main__":
    print "--------initialization testing------------"
    g2 = initialize([ ['Leftmost', 'Midleft','Midright','Rightmost']])
    print g2
    
    

