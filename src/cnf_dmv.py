# cnf_dmv.py

#import numpy # numpy provides Fast Arrays, for future optimization
import io

from common_dmv import *
SEALS = [GOR, RGOL, SEAL, NGOR, NRGOL] # overwriting here


if __name__ == "__main__":
    print "cnf_dmv module tests:"

def make_GO_AT(p_STOP,p_ATTACH):
    p_GO_AT = {}
    for (a,h,dir), p_ah in p_ATTACH.iteritems():
        p_GO_AT[a,h,dir, NON] = p_ah * (1-p_STOP[h, dir, NON])
        p_GO_AT[a,h,dir, ADJ] = p_ah * (1-p_STOP[h, dir, ADJ])
    return p_GO_AT

class CNF_DMV_Grammar(io.Grammar):
    '''The DMV-PCFG.

    Public members:
    p_STOP, p_ROOT, p_ATTACH, p_terminals
    These are changed in the Maximation step, then used to set the
    new probabilities of each CNF_DMV_Rule.

    __p_rules is private, but we can still say stuff like:
    for r in g.all_rules():
        r.prob = (1-p_STOP[...]) * p_ATTACH[...]
    '''
    def __str__(self):
        str = ""
        for r in self.all_rules():
            str += "%s\n" % r.__str__(self.numtag)
        return str

    def LHSs(self):
        return [ROOT] + [(s_h,h)
                         for h in self.headnums()
                         for s_h in SEALS]
    
    def sent_rules(self, sent_nums):
        sent_nums_stop = sent_nums + [POS(STOP)]
        return [ r for LHS in self.LHSs()
                   for r in self.arg_rules(LHS, sent_nums)
                 if  POS(r.L()) in sent_nums_stop
                 and POS(r.R()) in sent_nums_stop ]

    # used in outer:
    def mothersR(self, w_node, argnums):
        '''For all LHS and x, return all rules of the form 'LHS->x w_node'.'''
        if w_node not in self.__mothersR:
            self.__mothersR[w_node] = [r for LHS in self.LHSs()
                                       for r in self.rules(LHS)
                                       if r.R() == w_node]
        argnums.append(POS(STOP))
        return [r for r in self.__mothersR[w_node]
                if POS(r.L()) in argnums]

    def mothersL(self, w_node, argnums):
        '''For all LHS and x, return all rules of the form 'LHS->w_node x'.'''
        if w_node not in self.__mothersL:
            self.__mothersL[w_node] = [r for LHS in self.LHSs()
                                       for r in self.rules(LHS)
                                       if r.L() == w_node] 
        argnums.append(POS(STOP))
        return [r for r in self.__mothersL[w_node]
                if POS(r.R()) in argnums]

    
    # used in inner:
    def arg_rules(self, LHS, argnums):
        return [r for r in self.rules(LHS)
                if (POS(r.R()) in argnums
                    or POS(r.L()) in argnums)]


    def make_all_rules(self):
        self.new_rules([r for LHS in self.LHSs()
                        for r in self._make_rules(LHS, self.headnums())])

    def _make_rules(self, LHS, argnums):
        '''This is where the CNF grammar is defined. Also, s_dir_typ shows how
        useful it'd be to split up the seals into direction and
        type... todo?'''
        h = POS(LHS)
        if LHS == ROOT:
            return [CNF_DMV_Rule(LEFT, LHS, (SEAL,h), STOP, self.p_ROOT[h])
                    for h in set(argnums)]
        s_h = seals(LHS)
        if s_h == GOR:
            return [] # only terminals from here on
        s_dir_type = {  # seal of LHS
            RGOL: (RIGHT, 'STOP'), NGOR:  (RIGHT, 'ATTACH'),
            SEAL: (LEFT, 'STOP'),  NRGOL: (LEFT, 'ATTACH')  }
        dir_s_adj = { # seal of h_daughter
            RIGHT: [(GOR, True),(NGOR, False)] ,
            LEFT:  [(RGOL,True),(NRGOL,False)] }
        dir,type = s_dir_type[s_h]
        rule = {
            'ATTACH': [CNF_DMV_Rule(dir, LHS, (s, h), (SEAL,a), self.p_GO_AT[a,h,dir,adj])
                       for a in set(argnums) if (a,h,dir) in self.p_ATTACH
                       for s, adj in dir_s_adj[dir]] ,
            'STOP':   [CNF_DMV_Rule(dir, LHS, (s, h), STOP, self.p_STOP[h,dir,adj])
                       for s, adj in dir_s_adj[dir]] }
        return rule[type]
        

    def __init__(self, numtag, tagnum, p_ROOT, p_STOP, p_ATTACH, p_terminals):
        io.Grammar.__init__(self, numtag, tagnum, [], p_terminals)
        self.p_STOP = p_STOP
        self.p_ATTACH = p_ATTACH
        self.p_ROOT = p_ROOT
        self.p_GO_AT = make_GO_AT(self.p_STOP, self.p_ATTACH)
        self.make_all_rules()
        self.__mothersL = {}
        self.__mothersR = {}
        

class CNF_DMV_Rule(io.CNF_Rule):
    '''A single CNF rule in the PCFG, of the form 
    LHS -> L R
    where LHS, L and R are 'nodes', eg. of the form (seals, head).
    
    Public members:
    prob
    
    Private members:
    __L, __R, __LHS
    
    Different rule-types have different probabilities associated with
    them, see formulas.pdf
    '''
    def seals(self):
        return seals(self.LHS())
    
    def POS(self):
        return POS(self.LHS())
    
    def __init__(self, dir, LHS, h_daughter, a_daughter, prob):
        self.__dir = dir
        if dir == LEFT:
            L, R = a_daughter, h_daughter
        elif dir == RIGHT:
            L, R = h_daughter, a_daughter
        else:
            raise ValueError, "dir must be LEFT or RIGHT, given: %s"%dir
        for b_h in [LHS, L, R]:
            if seals(b_h) not in SEALS:
                raise ValueError("seals must be in %s; was given: %s"
                                 % (SEALS, seals(b_h)))
        io.CNF_Rule.__init__(self, LHS, L, R, prob)
    
    def adj(self):
        "'undefined' for ROOT"
        if self.__dir == LEFT:
            return seals(self.R()) == RGOL
        else: # RIGHT
            return seals(self.L()) == GOR

    def __str__(self, tag=lambda x:x):
        if self.adj(): adj_str = "adj"
        else: adj_str = "non_adj"
        if self.LHS() == ROOT: adj_str = ""
        return "%s --> %s %s\t[%.2f] %s" % (node_str(self.LHS(), tag),
                                            node_str(self.L(), tag),
                                            node_str(self.R(), tag),
                                            self.prob,
                                            adj_str)
    

    




###################################
# dmv-specific version of inner() #
###################################
def inner(i, j, LHS, g, sent, ichart={}):
    ''' A CNF rewrite of io.inner(), to take STOP rules into accord. '''
    def O(i,j):
        return sent[i]
    
    sent_nums = g.sent_nums(sent)
    
    def e(i,j,LHS, n_t):
        def tab():
            "Tabs for debug output"
            return "\t"*n_t
        if (i, j, LHS) in ichart:
            if 'INNER' in DEBUG:
                print "%s*= %.4f in ichart: i:%d j:%d LHS:%s" % (tab(), ichart[i, j, LHS], i, j, node_str(LHS))
            return ichart[i, j, LHS]
        else:
            # if seals(LHS) == RGOL then we have to STOP first
            if i == j-1 and seals(LHS) == GOR:
                if (LHS, O(i,j)) in g.p_terminals:
                    prob = g.p_terminals[LHS, O(i,j)] # "b[LHS, O(s)]" in Lari&Young
                else:
                    prob = 0.0 
                    if 'INNER' in DEBUG:
                        print "%sLACKING TERMINAL:" % tab()
                if 'INNER' in DEBUG:
                    print "%s*= %.4f (terminal: %s -> %s)" % (tab(),prob, node_str(LHS), O(i,j)) 
                return prob
            else:
                p = 0.0 # "sum over j,k in a[LHS,j,k]"
                for rule in g.arg_rules(LHS, sent_nums): 
                    if 'INNER' in DEBUG:
                        print "%ssumming rule %s i:%d j:%d" % (tab(),rule,i,j)
                    L = rule.L()
                    R = rule.R()
                    # if it's a STOP rule, rewrite for the same xrange:
                    if (L == STOP) or (R == STOP):
                        if L == STOP:
                            pLR = e(i, j, R, n_t+1)
                        elif R == STOP:
                            pLR = e(i, j, L, n_t+1)
                        p += rule.p() * pLR
                        if 'INNER' in DEBUG:
                            print "%sp= %.4f (STOP)" % (tab(), p) 
                            
                    elif j > i+1 and seals(LHS) != GOR:
                        # not a STOP, attachment rewrite:
                        for k in xtween(i, j): # i<k<j
                            p_L = e(i, k, L, n_t+1)
                            p_R = e(k, j, R, n_t+1)
                            p += rule.p() * p_L * p_R
                            if 'INNER' in DEBUG:
                                print "%sp= %.4f (ATTACH, p_L:%.4f, p_R:%.4f, rule:%.4f)" % (tab(), p,p_L,p_R,rule.p()) 
                ichart[i, j, LHS] = p
                return p
    # end of e-function
            
    inner_prob = e(i,j,LHS, 0)
    if 'INNER' in DEBUG:
        print debug_ichart(g,sent,ichart)
    return inner_prob
# end of cnf_dmv.inner(i, j, LHS, g, sent, ichart={})


def debug_ichart(g,sent,ichart):
    str = "---ICHART:---\n"
    for (i,j,LHS),v in ichart.iteritems():
        if type(v) == dict: # skip 'tree'
            continue
        str += "%s -> %s ... %s: \t%.4f\n" % (node_str(LHS,g.numtag),
                                              sent[i], sent[j-1], v)
    str += "---ICHART:end---\n"
    return str


def inner_sent(g, sent, ichart={}):
    return sum([inner(0, len(sent), ROOT, g, sent, ichart)]) 


#######################################
# cnf_dmv-specific version of outer() # 
#######################################
def outer(i,j,w_node, g, sent, ichart={}, ochart={}):
    def e(i,j,LHS):
        # or we could just look it up in ichart, assuming ichart to be done
        return inner(i, j, LHS, g, sent, ichart)
    
    sent_nums = g.sent_nums(sent)
    if POS(w_node) not in sent_nums[i:j]:
        # sanity check, w must be able to dominate sent[i:j]
        return 0.0
    
    def f(i,j,w_node):
        if (i,j,w_node) in ochart:
            return ochart[(i, j, w_node)]
        if w_node == ROOT:
            if i == 0 and j == len(sent):
                return 1.0
            else: # ROOT may only be used on full sentence
                return 0.0 # but we may have non-ROOTs over full sentence too

        p = 0.0

        for rule in g.mothersL(w_node, sent_nums): # rule.L() == w_node
            if 'OUTER' in DEBUG: print "w_node:%s (L) ; %s"%(node_str(w_node),rule)
            if rule.R() == STOP:
                p0 = f(i,j,rule.LHS()) * rule.p()
                if 'OUTER' in DEBUG: print p0
                p += p0
            else:
                for k in xgt(j,sent): # i<j<k
                    p0 = f(i,k, rule.LHS() ) * rule.p() * e(j,k, rule.R() )
                    if 'OUTER' in DEBUG: print p0
                    p += p0        

        for rule in g.mothersR(w_node, sent_nums): # rule.R() == w_node
            if 'OUTER' in DEBUG: print "w_node:%s (R) ; %s"%(node_str(w_node),rule)
            if rule.L() == STOP:
                p0 = f(i,j,rule.LHS()) * rule.p()
                if 'OUTER' in DEBUG: print p0
                p += p0
            else:
                for k in xlt(i): # k<i<j
                    p0 = e(k,i, rule.L() ) * rule.p() * f(k,j, rule.LHS() )
                    if 'OUTER' in DEBUG: print p0
                    p += p0

        ochart[i,j,w_node] = p
        return p

    
    return f(i,j,w_node)
# end outer(i,j,w_node, g,sent, ichart,ochart)



##############################
#      reestimation, todo:   #
##############################
def reest_zeros(rules):
    f = { ('den',ROOT) : 0.0 }
    for r in rules:
        for nd in ['num','den']:
            f[nd, r.LHS(), r.L(), r.R()] = 0.0
    return f

def reest_freq(g, corpus):
    ''' P_STOP(-STOP|...) = 1 - P_STOP(STOP|...) '''
    f = reest_zeros(g.all_rules())
    ichart = {}
    ochart = {}
    
    p_sent = None # 50 % speed increase on storing this locally

    def c_g(i,j,LHS,sent): 
        if p_sent == 0.0:
            return 0.0
        return e_g(i,j,LHS,sent) * f_g(i,j,LHS,sent) / p_sent
        
    def w1_g(i,j,rule,sent): # unary (stop) rules, LHS -> child_node
        if   rule.L() == STOP: child = rule.R()
        elif rule.R() == STOP: child = rule.L()
        else: raise ValueError, "expected a stop rule: %s"%(rule,)

        if p_sent == 0.0: return 0.0
        
        p_out = f_g(i,j,rule.LHS(),sent)
        if p_out == 0.0: return 0.0
        
        return rule.p() * e_g(i,j,child,sent) * p_out / p_sent

    def w_g(i,j,rule,sent):
        if p_sent == 0.0 or i+1 == j: return 0.0
        
        p_out = f_g(i,j,rule.LHS(),sent)
        if p_out == 0.0: return 0.0
        
        p = 0.0
        for k in xtween(i,j):
            p += rule.p() * e_g(i,k,rule.L(),sent) * e_g(k,j,rule.R(),sent) * p_out
        return p / p_sent

    def f_g(i,j,LHS,sent): 
        if (i,j,LHS) in ochart:
#             print ".",
            return ochart[i,j,LHS]
        else:
            return outer(i,j,LHS,g,sent,ichart,ochart)
        
    def e_g(i,j,LHS,sent): 
        if (i,j,LHS) in ichart:
#             print ".",
            return ichart[i,j,LHS]
        else:
            return inner(i,j,LHS,g,sent,ichart) 
        
    for s_num,sent in enumerate(corpus):
        if s_num%5==0: print "s.num %d"%s_num,
        if 'REEST' in DEBUG: print sent
        ichart = {}
        ochart = {}
        # since we keep re-using p_sent, it seems better to have
        # sentences as the outer loop; o/w we'd have to keep every chart
        p_sent = inner_sent(g, sent, ichart)
        
        sent_nums = g.sent_nums(sent)
        sent_rules = g.sent_rules(sent_nums)
        for r in sent_rules:
            LHS, L, R = r.LHS(), r.L(), r.R()
            if 'REEST' in DEBUG: print r
            if LHS == ROOT: 
                f['num',LHS,L,R] += r.p() * e_g(0, len(sent), R, sent) / p_sent
                continue # !!! o/w we add wrong values to it below
            if L == STOP or R == STOP:
                w = w1_g
            else:
                w = w_g
            for i in xlt(len(sent)):
                for j in xgt(i, sent):
                    f['num',LHS,L,R] +=   w(i,j, r,   sent)
                    f['den',LHS,L,R] += c_g(i,j, LHS, sent) # v_q
    print ""
    return f

def reestimate(g, corpus):
    f = reest_freq(g, corpus)
    print "applying f to rules"
    for r in g.all_rules():
        if r.LHS() == ROOT:
            r.prob = 1.0
        else:
            r.prob = f['den',r.LHS(),r.L(),r.R()]
        if r.prob > 0.0:
            r.prob = f['num',r.LHS(),r.L(),r.R()] / r.prob
    return g


##############################
#     Testing functions:     #
##############################
def testgrammar():
    # make sure we use the same data:
    from loc_h_dmv import testcorpus
    
    import cnf_harmonic
    reload(cnf_harmonic)
    return cnf_harmonic.initialize(testcorpus)

def testreestimation():
    from loc_h_dmv import testcorpus
    g = testgrammar()
    g = reestimate(g, testcorpus[0:4])
    return g

def testgrammar_a():                            # Non, Adj
    _h_ = CNF_DMV_Rule((SEAL,0), STOP,    ( RGOL,0), 1.0, 1.0) # LSTOP
    h_S = CNF_DMV_Rule(( RGOL,0),(GOR,0),  STOP,    0.4, 0.3) # RSTOP
    h_A = CNF_DMV_Rule(( RGOL,0),(SEAL,0),( RGOL,0),0.2, 0.1) # Lattach
    h_Aa= CNF_DMV_Rule(( RGOL,0),(SEAL,1),( RGOL,0),0.4, 0.6) # Lattach to a
    h   = CNF_DMV_Rule((GOR,0),(GOR,0),(SEAL,0),    1.0, 1.0) # Rattach
    ha  = CNF_DMV_Rule((GOR,0),(GOR,0),(SEAL,1),    1.0, 1.0) # Rattach to a
    rh  = CNF_DMV_Rule(   ROOT,   STOP,    (SEAL,0),  0.9, 0.9) # ROOT

    _a_ = CNF_DMV_Rule((SEAL,1), STOP,    ( RGOL,1), 1.0, 1.0) # LSTOP
    a_S = CNF_DMV_Rule(( RGOL,1),(GOR,1),  STOP,    0.4, 0.3) # RSTOP
    a_A = CNF_DMV_Rule(( RGOL,1),(SEAL,1),( RGOL,1),0.4, 0.6) # Lattach
    a_Ah= CNF_DMV_Rule(( RGOL,1),(SEAL,0),( RGOL,1),0.2, 0.1) # Lattach to h
    a   = CNF_DMV_Rule((GOR,1),(GOR,1),(SEAL,1),    1.0, 1.0) # Rattach
    ah  = CNF_DMV_Rule((GOR,1),(GOR,1),(SEAL,0),    1.0, 1.0) # Rattach to h
    ra  = CNF_DMV_Rule(   ROOT,   STOP,    (SEAL,1),  0.1, 0.1) # ROOT

    p_rules = [ h_Aa, ha, a_Ah, ah, ra, _a_, a_S, a_A, a, rh, _h_, h_S, h_A, h ]

    
    b  = {}
    b[(GOR, 0), 'h'] = 1.0
    b[(GOR, 1), 'a'] = 1.0
    
    return CNF_DMV_Grammar({0:'h',1:'a'}, {'h':0,'a':1},
                           None,None,None,b)

def testgrammar_h():
    h = 0
    p_ROOT, p_STOP, p_ATTACH, p_ORDER = {},{},{},{}
    p_ROOT[h] = 1.0
    p_STOP[h,LEFT,NON] = 1.0
    p_STOP[h,LEFT,ADJ] = 1.0
    p_STOP[h,RIGHT,NON] = 0.4
    p_STOP[h,RIGHT,ADJ] = 0.3
    p_ATTACH[h,h,LEFT] = 1.0  # not used
    p_ATTACH[h,h,RIGHT] = 1.0 # not used
    p_terminals  = {}
    p_terminals[(GOR, 0), 'h'] = 1.0

    g = CNF_DMV_Grammar({h:'h'}, {'h':h}, p_ROOT, p_STOP, p_ATTACH, p_terminals)

    g.p_GO_AT[h,h,LEFT,NON] = 0.6 # these probabilities are impossible
    g.p_GO_AT[h,h,LEFT,ADJ] = 0.7 # so add them manually...
    g.p_GO_AT[h,h,RIGHT,NON] = 1.0
    g.p_GO_AT[h,h,RIGHT,ADJ] = 1.0
    g.make_all_rules()
    return g
    

def testreestimation_h():
    DEBUG.add('REEST')
    g = testgrammar_h()
    return reestimate(g,['h h h'.split()])

def regression_tests():
    test("0.1830", # = .120 + .063, since we have no loc_h
         "%.4f" % inner(0, 2, (SEAL,0), testgrammar_h(), 'h h'.split(), {}))
        
    test("0.1842", # = .0498 + .1092 +.0252 
         "%.4f" % inner(0, 3, (SEAL,0), testgrammar_h(), 'h h h'.split(), {}))
    test("0.1842",
         "%.4f" % inner_sent(testgrammar_h(), 'h h h'.split()))

    test("0.61" ,
         "%.2f" % outer(1, 3, ( RGOL,0), testgrammar_h(),'h h h'.split(),{},{}))
    test("0.58" ,
         "%.2f" % outer(1, 3, (NRGOL,0), testgrammar_h(),'h h h'.split(),{},{}))
    
    
if __name__ == "__main__":
    DEBUG.clear()

#     import profile
#     profile.run('testreestimation()')

#    DEBUG.add('reest_attach')
#     import timeit
#     print timeit.Timer("cnf_dmv.testreestimation_h()",'''import cnf_dmv
# reload(cnf_dmv)''').timeit(1)

if __name__ == "__main__":
    regression_tests()
#     g = testgrammar()
#     print g

