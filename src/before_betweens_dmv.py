# before_betweens_dmv.py
# 
# dmv reestimation and inside-outside probabilities using loc_h, but
# at-word sentence locations

#import numpy # numpy provides Fast Arrays, for future optimization
import io
from common_dmv import *

if __name__ == "__main__":
    print "before_betweens_dmv module tests:"

class DMV_Grammar(io.Grammar):
    '''The DMV-PCFG.

    Public members:
    p_STOP, p_ROOT, p_CHOOSE, p_terminals
    These are changed in the Maximation step, then used to set the
    new probabilities of each DMV_Rule.

    Todo: make p_terminals private? (But it has to be changable in
    maximation step due to the short-cutting rules... could of course
    make a DMV_Grammar function to update the short-cut rules...)

    __p_rules is private, but we can still say stuff like:
    for r in g.all_rules():
        r.probN = newProbN
    
    What other representations do we need? (P_STOP formula uses
    deps_D(h,l/r) at least)'''
    def __str__(self):
        str = ""
        for r in self.all_rules():
             str += "%s\n" % r.__str__(self.numtag)
        return str

    def h_rules(self, h):
        return [r for r in self.all_rules() if r.POS() == h]
    
    def mothersL(self, Node, sent_nums, loc_N):
        # todo: speed-test with and without sent_nums/loc_N cut-off
        return [r for r in self.all_rules() if r.L() == Node
                and (POS(r.R()) in sent_nums[loc_N+1:] or r.R() == STOP)]
    
    def mothersR(self, Node, sent_nums, loc_N):
        return [r for r in self.all_rules() if r.R() == Node
                and (POS(r.L()) in sent_nums[:loc_N] or r.L() == STOP)]

    def rules(self, LHS):
        return [r for r in self.all_rules() if r.LHS() == LHS]
    
    def sent_rules(self, LHS, sent_nums):
        '''Used in dmv.inner. Todo: this takes a _lot_ of time, it
        seems. Could use some more space and cache some of this
        somehow perhaps?'''
        # We don't want to rule out STOPs!
        nums = sent_nums + [ POS(STOP) ]
        return [r for r in self.all_rules() if r.LHS() == LHS
                and POS(r.L()) in nums and POS(r.R()) in nums]
    
    def deps_L(self, head): # todo: do I use this at all?
        # todo test, probably this list comprehension doesn't work 
        return [a for r in self.all_rules() if r.POS() == head and a == r.L()]
    
    def deps_R(self, head):
        # todo test, probably this list comprehension doesn't work 
        return [a for r in self.all_rules() if r.POS() == head and a == r.R()]
    
    def __init__(self, numtag, tagnum, p_rules, p_terminals, p_STOP, p_CHOOSE, p_ROOT):
        io.Grammar.__init__(self, numtag, tagnum, p_rules, p_terminals)
        self.p_STOP = p_STOP
        self.p_CHOOSE = p_CHOOSE
        self.p_ROOT = p_ROOT
        self.head_nums = [k for k in numtag.iterkeys()]
        

class DMV_Rule(io.CNF_Rule):
    '''A single CNF rule in the PCFG, of the form 
    LHS -> L R
    where LHS, L and R are 'nodes', eg. of the form (seals, head).
    
    Public members:
    probN, probA
    
    Private members:
    __L, __R, __LHS
    
    Different rule-types have different probabilities associated with
    them:

    _h_ -> STOP  h_     P( STOP|h,L,    adj)
    _h_ -> STOP  h_     P( STOP|h,L,non_adj)
     h_ ->  h  STOP     P( STOP|h,R,    adj)
     h_ ->  h  STOP     P( STOP|h,R,non_adj)
     h_ -> _a_   h_     P(-STOP|h,L,    adj) * P(a|h,L)
     h_ -> _a_   h_     P(-STOP|h,L,non_adj) * P(a|h,L)
     h  ->  h   _a_     P(-STOP|h,R,    adj) * P(a|h,R)
     h  ->  h   _a_     P(-STOP|h,R,non_adj) * P(a|h,R) 
    '''
    def p(self, adj, *arg):
        if adj:
            return self.probA
        else:
            return self.probN

    def adj(middle, loc_h):
        "middle is eg. k when rewriting for i<k<j (inside probabilities)."
        return middle == loc_h[0] or middle == loc_h[1]

    def p_STOP(self, s, t, loc_h):
        '''Returns the correct probability, adjacent if we're rewriting from
        the (either left or right) end of the fragment.
        '''
        if self.L() == STOP:
            return self.p(s == loc_h)
        elif self.R() == STOP:
            if not loc_h == s:
                if 'TODO' in DEBUG:
                    print "(%s given loc_h:%d but s:%d. Todo: optimize away!)" % (self, loc_h, s) 
                return 0.0
            else:
                return self.p(t == loc_h)
            
    def p_ATTACH(self, r, loc_h, s=None):
        '''Returns the correct probability, adjacent if we haven't attached
        anything before.
        (This is actually p_choose*(1-p_stop).)'''
        if self.LHS() == self.L():
            if s and not loc_h == s:
                if 'TODO' in DEBUG:
                    print "(%s given loc_h (loc_L):%d but s:%d. Todo: optimize away!)" % (self, loc_h, s) 
                return 0.0
            else:
                return self.p(r == loc_h)
        elif self.LHS() == self.R():
            return self.p(r+1 == loc_h)
        
    def seals(self):
        return seals(self.LHS())
    
    def POS(self):
        return POS(self.LHS())
    
    def __init__(self, LHS, L, R, probN, probA):
        for b_h in [LHS, L, R]:
            if seals(b_h) not in SEALS:
                raise ValueError("seals must be in %s; was given: %s"
                                 % (SEALS, seals(b_h)))
        io.CNF_Rule.__init__(self, LHS, L, R, probN)
        self.probA = probA # adjacent
        self.probN = probN # non_adj
        
    @classmethod # so we can call DMV_Rule.bar_str(b_h) 
    def bar_str(cls, b_h, tag=lambda x:x):
        if(b_h == ROOT):
            return 'ROOT'
        elif(b_h == STOP):
            return 'STOP'
        elif(seals(b_h) == RGOL):
            return " %s_ " % tag(POS(b_h))
        elif(seals(b_h) == SEAL):
            return "_%s_ " % tag(POS(b_h))
        else:
            return " %s  " % tag(POS(b_h))

    
    def __str__(self, tag=lambda x:x):
        return "%s-->%s %s\t[N %.2f] [A %.2f]" % (self.bar_str(self.LHS(), tag),
                                                  self.bar_str(self.L(), tag),
                                                  self.bar_str(self.R(), tag),
                                                  self.probN,
                                                  self.probA)
    

    




###################################
# dmv-specific version of inner() #
###################################
def locs(h, sent, s=0, t=None, remove=None):
    '''Return the locations of h in sent, or some fragment of sent (in the
    latter case we make sure to offset the locations correctly so that
    for any x in the returned list, sent[x]==h).

    t is inclusive, to match the way indices work with inner()
    (although python list-splicing has "exclusive" end indices)'''
    if t == None:
        t = len(sent)-1
    return [i+s for i,w in enumerate(sent[s:t+1])
            if w == h and not (i+s) == remove]


def inner(s, t, LHS, loc_h, g, sent, ichart={}):
    ''' A rewrite of io.inner(), to take adjacency into accord.

    The ichart is now of this form:
    ichart[s,t,LHS, loc_h]
    
    loc_h gives adjacency (along with r and location of other child
    for attachment rules), and is needed in P_STOP reestimation.
    
    Todo: if possible, refactor (move dmv-specific stuff back into
    dmv, so this is "general" enough to be in io.py)
    '''
    
    def O(s):
        return sent[s]
    
    sent_nums = g.sent_nums(sent)
    
    def e(s,t,LHS, loc_h, n_t):
        def tab():
            "Tabs for debug output"
            return "\t"*n_t
        
        if (s, t, LHS, loc_h) in ichart:
            if 'INNER' in DEBUG:
                print "%s*= %.4f in ichart: s:%d t:%d LHS:%s loc:%d" % (tab(),ichart[s, t, LHS, loc_h], s, t,
                                                                       DMV_Rule.bar_str(LHS), loc_h)
            return ichart[s, t, LHS, loc_h]
        else:
            if s == t and seals(LHS) == GOR:
                if not loc_h == s:
                    if 'INNER' in DEBUG:
                        print "%s*= 0.0 (wrong loc_h)" % tab()
                    return 0.0
                elif (LHS, O(s)) in g.p_terminals:
                    prob = g.p_terminals[LHS, O(s)] # "b[LHS, O(s)]" in Lari&Young
                else:
                    # todo: assuming this is how to deal w/lacking
                    # rules, since we add prob.s, and 0 is identity
                    prob = 0.0 
                    if 'INNER' in DEBUG:
                        print "%sLACKING TERMINAL:" % tab()
                # todo: add to ichart perhaps? Although, it _is_ simple lookup..
                if 'INNER' in DEBUG:
                    print "%s*= %.4f (terminal: %s -> %s_%d)" % (tab(),prob, DMV_Rule.bar_str(LHS), O(s), loc_h) 
                return prob
            else:
                p = 0.0 # "sum over j,k in a[LHS,j,k]"
                for rule in g.sent_rules(LHS, sent_nums): 
                    if 'INNER' in DEBUG:
                        print "%ssumming rule %s s:%d t:%d loc:%d" % (tab(),rule,s,t,loc_h) 
                    L = rule.L()
                    R = rule.R()
                    if loc_h == t and LHS == L:
                        continue # todo: speed-test
                    if loc_h == s and LHS == R:
                        continue 
                    # if it's a STOP rule, rewrite for the same xrange:
                    if (L == STOP) or (R == STOP):
                        if L == STOP:
                            pLR = e(s, t, R, loc_h, n_t+1)
                        elif R == STOP:
                            pLR = e(s, t, L, loc_h, n_t+1)
                        p += rule.p_STOP(s, t, loc_h) * pLR
                        if 'INNER' in DEBUG:
                            print "%sp= %.4f (STOP)" % (tab(), p) 
                            
                    elif t > s: # not a STOP, attachment rewrite:
                        rp_ATTACH = rule.p_ATTACH # todo: profile/speedtest
                        for r in xrange(s, t):
                            p_h = rp_ATTACH(r, loc_h, s=s)
                            if LHS == L: 
                                locs_L = [loc_h]
                                locs_R = locs(POS(R), sent_nums, r+1, t, loc_h)
                            elif LHS == R: 
                                locs_L = locs(POS(L), sent_nums,  s,  r, loc_h)
                                locs_R = [loc_h]
                            for loc_L in locs_L:
                                pL = e(s, r, L, loc_L, n_t+1)
                                if pL > 0.0: 
                                    for loc_R in locs_R:
                                        pR = e(r+1, t, R, loc_R, n_t+1)
                                        p += pL * p_h * pR
                            if 'INNER' in DEBUG:
                                print "%sp= %.4f (ATTACH)" % (tab(), p) 
                ichart[s, t, LHS, loc_h] = p
                return p
    # end of e-function
            
    inner_prob = e(s,t,LHS,loc_h, 0)
    if 'INNER' in DEBUG:
        print debug_ichart(g,sent,ichart)
    return inner_prob
# end of dmv.inner(s, t, LHS, loc_h, g, sent, ichart={})


def debug_ichart(g,sent,ichart):
    str = "---ICHART:---\n"
    for (s,t,LHS,loc_h),v in ichart.iteritems():
        if type(v) == dict: # skip 'tree'
            continue
        str += "%s -> %s_%d ... %s_%d (loc_h:%s):\t%.4f\n" % (DMV_Rule.bar_str(LHS,g.numtag),
                                                              sent[s], s, sent[s], t, loc_h, v)
    str += "---ICHART:end---\n"
    return str


def inner_sent(g, sent, ichart={}):
    return sum([inner(0, len(sent)-1, ROOT, loc_h, g, sent, ichart)
                for loc_h in xrange(len(sent))])


###################################
# dmv-specific version of outer() #
###################################
def outer(s,t,Node,loc_N, g, sent, ichart={}, ochart={}):
    ''' http://www.student.uib.no/~kun041/dmvccm/DMVCCM.html#outer
    '''
    def e(s,t,LHS,loc_h):
        # or we could just look it up in ichart, assuming ichart to be done
        return inner(s, t, LHS, loc_h, g, sent, ichart)
    
    T = len(sent)-1
    sent_nums = g.sent_nums(sent)
    
    def f(s,t,Node,loc_N):
        if (s,t,Node,loc_N) in ochart:
            return ochart[(s, t, Node,loc_N)]
        if Node == ROOT:
            if s == 0 and t == T:
                return 1.0
            else: # ROOT may only be used on full sentence
                return 0.0 # but we may have non-ROOTs over full sentence too
        p = 0.0
        
        for mom in g.mothersL(Node, sent_nums, loc_N): # mom.L() == Node
            R = mom.R()
            mLHS = mom.LHS()
            if R == STOP:
                p += f(s,t,mLHS,loc_N) * mom.p_STOP(s,t,loc_N) # == loc_m
            else:
                if seals(mLHS) == RGOL: # left attachment, POS(mLHS) == POS(R)
                    for r in xrange(t+1,T+1): # t+1 to lasT 
                        for loc_m in locs(POS(mLHS),sent_nums,t+1,r):
                            p_m = mom.p(t+1 == loc_m)
                            p += f(s,r,mLHS,loc_m) * p_m * e(t+1,r,R,loc_m)
                elif seals(mLHS) == GOR: # right attachment, POS(mLHS) == POS(Node)
                    loc_m = loc_N
                    p_m = mom.p( t  == loc_m)
                    for r in xrange(t+1,T+1): # t+1 to lasT 
                        for loc_R in locs(POS(R),sent_nums,t+1,r):
                            p += f(s,r,mLHS,loc_m) * p_m * e(t+1,r,R,loc_R)
        
        for mom in g.mothersR(Node, sent_nums, loc_N): # mom.R() == Node
            L = mom.L()
            mLHS = mom.LHS()
            if L == STOP:
                p += f(s,t,mLHS,loc_N) * mom.p_STOP(s,t,loc_N) # == loc_m
            else:
                if seals(mLHS) == RGOL: # left attachment, POS(mLHS) == POS(Node)
                    loc_m = loc_N
                    p_m = mom.p( s  == loc_m)
                    for r in xrange(0,s): # first to s-1 
                        for loc_L in locs(POS(L),sent_nums,r,s-1):
                            p += e(r,s-1,L, loc_L) * p_m * f(r,t,mLHS,loc_m)
                elif seals(mLHS) == GOR: # right attachment, POS(mLHS) == POS(L)
                    for r in xrange(0,s): # first to s-1
                        for loc_m in locs(POS(mLHS),sent_nums,r,s-1): 
                            p_m = mom.p(s-1 == loc_m)
                            p += e(r,s-1,L, loc_m) * p_m * f(r,t,mLHS,loc_m)
        ochart[s,t,Node,loc_N] = p
        return p

    
    return f(s,t,Node,loc_N)
# end outer(s,t,Node,loc_N, g,sent, ichart,ochart)



##############################
#      reestimation, todo:   #
##############################
## using local version instead
# def c(s,t,LHS,loc_h,g,sent,ichart={},ochart={}):
#     # assuming P_sent = P(D(ROOT)) = inner(sent). todo: check K&M about this
#     p_sent = inner_sent(g, sent, ichart)
#     p_in = inner(s,t,LHS,loc_h,g,sent,ichart) 
#     p_out = outer(s,t,LHS,loc_h,g,sent,ichart,ochart)
#     if p_sent > 0.0:
#         return p_in * p_out / p_sent
#     else:
#         return p_sent

def reest_zeros(h_nums):
    # todo: p_ROOT? ... p_terminals?
    f = {}
    for h in h_nums:
        for stop in ['LNSTOP','LASTOP','RNSTOP','RASTOP']:
            for nd in ['num','den']:
                f[stop,nd,h] = 0.0
        for choice in ['RCHOOSE', 'LCHOOSE']:
            f[choice,'den',h] = 0.0
    return f

def reest_freq(g, corpus):
    ''' P_STOP(-STOP|...) = 1 - P_STOP(STOP|...) '''
    f = reest_zeros(g.head_nums)
    ichart = {}
    ochart = {}
    
    p_sent = None # 50 % speed increase on storing this locally
    def c_g(s,t,LHS,loc_h,sent): # altogether 2x faster than the global c()
        if (s,t,LHS,loc_h) in ichart:
            p_in = ichart[s,t,LHS,loc_h]
        else:
            p_in = inner(s,t,LHS,loc_h,g,sent,ichart) 
        if (s,t,LHS,loc_h) in ochart:
            p_out = ochart[s,t,LHS,loc_h]
        else:
            p_out = outer(s,t,LHS,loc_h,g,sent,ichart,ochart)

        if p_sent > 0.0:
            return p_in * p_out / p_sent
        else:
            return p_sent

    def w_g(s,t,a,loc_a,LHS,loc_h,sent):
        "Todo: should sum through all r in between s and t in sent(_nums)"
        h = POS(LHS)
        b_h = seals(LHS)
        if b_h == GOR:
            return e_L * e_R * f_g(s,t,(GOR, h), loc_h, sent) * p_g(r,(GOR, h), (GOR, h), (SEAL, a), loc_h, sent_nums)
        if b_h == RGOL:
            return e_L * e_R * f_g(s,t,(RGOL, h), loc_h, sent) * p_g(r,(RGOL, h),(SEAL, a),(RGOL, h),loc_h,sent_nums)

    def f_g(s,t,LHS,loc_h,sent): # todo: test with choose rules
        if (s,t,LHS,loc_h) in ochart:
            return ochart[s,t,LHS,loc_h]
        else:
            return outer(s,t,LHS,loc_h,g,sent,ichart,ochart)

    def e_g(s,t,LHS,loc_h,sent): # todo: test with choose rules
        if (s,t,LHS,loc_h) in ichart:
            return ichart[s,t,LHS,loc_h]
        else:
            return inner(s,t,LHS,loc_h,g,sent,ichart) 
        
    def p_g(r,LHS,L,R,loc_h,sent):
        rules = [rule for rule in g.sent_rules(LHS, sent)
                 if rule.L() == L and rule.R() == R]
        rule = rules[0]
        if len(rules) > 1:
            raise Exception("Several rules matching a[i,j,k]")
        return rule.p_ATTACH(r,loc_h)

    for sent in corpus:
        if 'reest' in DEBUG:
            print sent
        ichart = {}
        ochart = {}
        p_sent = inner_sent(g, sent, ichart)

        sent_nums = g.sent_nums(sent)
        # todo: use sum([ichart[s, t...] etc? but can we then
        # keep den and num separate within _one_ sum()-call?
        for loc_h,h in enumerate(sent_nums):
            for t in xrange(loc_h, len(sent)):
                for s in xrange(loc_h): # s<loc(h), xrange gives strictly less
                    # left non-adjacent stop:
                    f['LNSTOP','num',h] += c_g(s, t, (SEAL, h), loc_h,sent)
                    f['LNSTOP','den',h] += c_g(s, t, (RGOL,h), loc_h,sent)
                # left adjacent stop:
                f['LASTOP','num',h] += c_g(loc_h, t, (SEAL, h), loc_h,sent)
                f['LASTOP','den',h] += c_g(loc_h, t, (RGOL,h), loc_h,sent)
            for t in xrange(loc_h+1, len(sent)):
                # right non-adjacent stop:
                f['RNSTOP','num',h] += c_g(loc_h, t, (RGOL,h), loc_h,sent)
                f['RNSTOP','den',h] += c_g(loc_h, t, (GOR, h), loc_h,sent)
            # right adjacent stop:
            f['RASTOP','num',h] += c_g(loc_h, loc_h, (RGOL,h), loc_h,sent)
            f['RASTOP','den',h] += c_g(loc_h, loc_h, (GOR, h), loc_h,sent)

            # right attachment:  TODO: try with p*e*e*f instead of c, for numerator
            if 'reest_attach' in DEBUG:
                print "Rattach %s: for t in %s"%(g.numtag(h),sent[loc_h+1:len(sent)])
            for t in xrange(loc_h+1, len(sent)): 
                cM = c_g(loc_h,t,(GOR, h), loc_h, sent) # v_q in L&Y 
                f['RCHOOSE','den',h] += cM
                if 'reest_attach' in DEBUG:
                    print "\tc_g( %d , %d, %s, %s, sent)=%.4f"%(loc_h,t,g.numtag(h),loc_h,cM)
                args = {} # for summing w_q's in L&Y, without 1/P_q
                for r in xrange(loc_h+1, t+1): # loc_h < r <= t 
                    e_L = e_g(loc_h, r-1, (GOR, h), loc_h, sent)
                    if 'reest_attach' in DEBUG:
                        print "\t\te_g( %d , %d, %s, %d, sent)=%.4f"%(loc_h,r-1,g.numtag(h),loc_h,e_L)
                    for i,a in enumerate(sent_nums[r:t+1]):
                        loc_a = i+r
                        e_R = e_g(r, t, (SEAL, a), loc_a, sent)
                        if a not in args:
                            args[a] = 0.0
                        args[a] += e_L * e_R * f_g(loc_h,t,(GOR, h), loc_h, sent) * p_g(r,(GOR, h), (GOR, h), (SEAL, a), loc_h, sent_nums)
                    for a,sum_a in args.iteritems():
                        f['RCHOOSE','num',h,a] = sum_a / p_sent
                        

            # left attachment:
            if 'reest_attach' in DEBUG:
                print "Lattach %s: for s in %s"%(g.numtag(h),sent[0:loc_h])
            for s in xrange(0, loc_h):
                if 'reest_attach' in DEBUG:
                    print "\tfor t in %s"%sent[loc_h:len(sent)]
                for t in xrange(loc_h, len(sent)):
                    c_M = c_g(s,t,(RGOL, h), loc_h, sent) # v_q in L&Y 
                    f['LCHOOSE','den',h] += c_M
                    if 'reest_attach' in DEBUG:
                        print "\t\tc_g( %d , %d, %s_, %s, sent)=%.4f"%(s,t,g.numtag(h),loc_h,c_M)
                    if 'reest_attach' in DEBUG:
                        print "\t\tfor r in %s"%(sent[s:loc_h])
                    args = {} # for summing w_q's in L&Y, without 1/P_q
                    for r in xrange(s, loc_h): # s <= r < loc_h <= t
                        e_R = e_g(r+1, t, (RGOL, h), loc_h, sent)
                        if 'reest_attach' in DEBUG:
                            print "\t\te_g( %d , %d, %s_, %d, sent)=%.4f"%(r+1,t,g.numtag(h),loc_h,e_R)
                        for i,a in enumerate(sent_nums[s:r+1]):
                            loc_a = i+s
                            e_L = e_g( s , r, (SEAL, a), loc_a, sent)
                            if a not in args:
                                args[a] = 0.0
                            args[a] += e_L * e_R * f_g(s,t,(RGOL, h), loc_h, sent) * p_g(r,(RGOL, h),(SEAL, a),(RGOL, h),loc_h,sent_nums)
                    for a,sum_a in args.iteritems():
                        f['LCHOOSE', 'num',h,a] = sum_a / p_sent 
    return f

def reestimate(g, corpus):
    ""
    f = reest_freq(g, corpus)
    # we want to go through only non-ROOT left-STOPs.. 
    for r in g.all_rules():
        reest_rule(r,f, g)
    return f


def reest_rule(r,f, g): # g just for numtag / debug output, remove eventually?
    "remove 0-prob rules? todo"
    h = r.POS()
    if r.LHS() == ROOT:
        return None # not sure what todo yet here
    if r.L() == STOP or POS(r.R()) == h:
        dir = 'L'
    elif r.R() == STOP or POS(r.L()) == h:
        dir = 'R'
    else:
        raise Exception("Odd rule in reestimation.")

    p_stopN = f[dir+'NSTOP','den',h]
    if p_stopN > 0.0:
        p_stopN = f[dir+'NSTOP','num',h] / p_stopN

    p_stopA = f[dir+'ASTOP','den',h]
    if p_stopA > 0.0:
        p_stopA = f[dir+'ASTOP','num',h] / p_stopA

    if r.L() == STOP or r.R() == STOP: # stop rules
        if 'reest' in DEBUG:
            print "p(STOP|%d=%s,%s,N): %.4f (was: %.4f)"%(h,g.numtag(h),dir, p_stopN, r.probN) 
            print "p(STOP|%d=%s,%s,A): %.4f (was: %.4f)"%(h,g.numtag(h),dir, p_stopA, r.probA) 
        r.probN = p_stopN
        r.probA = p_stopA

    else: # attachment rules
        pchoose = f[dir+'CHOOSE','den',h]
        if pchoose > 0.0:
            if POS(r.R()) == h: # left attachment
                a = POS(r.L())
            elif POS(r.L()) == h: # right attachment
                a = POS(r.R())
            pchoose = f[dir+'CHOOSE','num',h,a] / pchoose 
            r.probN = (1-p_stopN) * pchoose
            r.probA = (1-p_stopA) * pchoose
            if 'reest' in DEBUG:
                print "p(%d=%s|%d=%s,%s): %.4f,\tprobN: %.4f, probA: %.4f"%(a,g.numtag(a),h,g.numtag(h),dir, pchoose,r.probN,r.probA) 







##############################
#     testing functions:     #
##############################

testcorpus = [s.split() for s in ['det nn vbd c vbd','vbd nn c vbd',
                                  'det nn vbd',      'det nn vbd c pp', 
                                  'det nn vbd',      'det vbd vbd c pp', 
                                  'det nn vbd',      'det nn vbd c vbd', 
                                  'det nn vbd',      'det nn vbd c vbd', 
                                  'det nn vbd',      'det nn vbd c vbd', 
                                  'det nn vbd',      'det nn vbd c pp', 
                                  'det nn vbd pp',   'det nn vbd', ]]

def testgrammar():
    import before_betweens_harmonic
    reload(before_betweens_harmonic)
    return before_betweens_harmonic.initialize(testcorpus)

def testreestimation():
    g = testgrammar()
    f = reestimate(g, testcorpus)
    f_stops = {('LNSTOP', 'den', 3): 12.212773236178391, ('RASTOP', 'den', 2): 4.0, ('RNSTOP', 'num', 4): 2.5553487221351365, ('LNSTOP', 'den', 2): 1.274904052793207, ('LASTOP', 'num', 1): 14.999999999999995, ('RASTOP', 'den', 3): 15.0, ('LASTOP', 'num', 4): 16.65701084787457, ('LASTOP', 'num', 0): 4.1600647714443468, ('LNSTOP', 'den', 4): 6.0170669155897105, ('LASTOP', 'num', 3): 2.7872267638216113, ('LASTOP', 'num', 2): 2.9723139990470515, ('LASTOP', 'den', 2): 4.0, ('RNSTOP', 'den', 3): 12.945787931730905, ('LASTOP', 'den', 3): 14.999999999999996, ('RNSTOP', 'den', 2): 0.0, ('LASTOP', 'den', 0): 8.0, ('RASTOP', 'num', 4): 19.44465127786486, ('RNSTOP', 'den', 1): 3.1966410324085777, ('LASTOP', 'den', 1): 14.999999999999995, ('RASTOP', 'num', 3): 4.1061665495365558, ('RNSTOP', 'den', 0): 4.8282499043902476, ('LNSTOP', 'num', 4): 5.3429891521254289, ('RASTOP', 'num', 2): 4.0, ('LASTOP', 'den', 4): 22.0, ('RASTOP', 'num', 1): 12.400273895299103, ('LNSTOP', 'num', 2): 1.0276860009529487, ('RASTOP', 'num', 0): 3.1717500956097533, ('LNSTOP', 'num', 3): 12.212773236178391, ('RASTOP', 'den', 4): 22.0, ('RNSTOP', 'den', 4): 2.8705211946979836, ('LNSTOP', 'num', 0): 3.8399352285556518, ('LNSTOP', 'num', 1): 0.0, ('RNSTOP', 'num', 0): 4.8282499043902476, ('RNSTOP', 'num', 1): 2.5997261047008959, ('LNSTOP', 'den', 1): 0.0, ('RASTOP', 'den', 0): 8.0, ('RNSTOP', 'num', 2): 0.0, ('LNSTOP', 'den', 0): 4.6540557322109795, ('RASTOP', 'den', 1): 15.0, ('RNSTOP', 'num', 3): 10.893833450463443}
    for k,v in f_stops.iteritems():
        if not k in f:
            print '''Regression!(?) Something changed in the P_STOP reestimation,
expected f[%s]=%.4f, but %s not in f'''%(k,v,k)
            pass
        elif not "%.10f"%f[k] == "%.10f"%v:
            print '''Regression!(?) Something changed in the P_STOP reestimation,
expected f[%s]=%.4f, got f[%s]=%.4f.'''%(k,v,k,f[k])
            pass


def testgrammar_a():                            # Non, Adj
    _h_ = DMV_Rule((SEAL,0), STOP,    ( RGOL,0), 1.0, 1.0) # LSTOP
    h_S = DMV_Rule(( RGOL,0),(GOR,0),  STOP,    0.4, 0.3) # RSTOP
    h_A = DMV_Rule(( RGOL,0),(SEAL,0),( RGOL,0),0.2, 0.1) # Lattach
    h_Aa= DMV_Rule(( RGOL,0),(SEAL,1),( RGOL,0),0.4, 0.6) # Lattach to a
    h   = DMV_Rule((GOR,0),(GOR,0),(SEAL,0),    1.0, 1.0) # Rattach
    ha  = DMV_Rule((GOR,0),(GOR,0),(SEAL,1),    1.0, 1.0) # Rattach to a
    rh  = DMV_Rule(   ROOT,   STOP,    (SEAL,0),  0.9, 0.9) # ROOT

    _a_ = DMV_Rule((SEAL,1), STOP,    ( RGOL,1), 1.0, 1.0) # LSTOP
    a_S = DMV_Rule(( RGOL,1),(GOR,1),  STOP,    0.4, 0.3) # RSTOP
    a_A = DMV_Rule(( RGOL,1),(SEAL,1),( RGOL,1),0.4, 0.6) # Lattach
    a_Ah= DMV_Rule(( RGOL,1),(SEAL,0),( RGOL,1),0.2, 0.1) # Lattach to h
    a   = DMV_Rule((GOR,1),(GOR,1),(SEAL,1),    1.0, 1.0) # Rattach
    ah  = DMV_Rule((GOR,1),(GOR,1),(SEAL,0),    1.0, 1.0) # Rattach to h
    ra  = DMV_Rule(   ROOT,   STOP,    (SEAL,1),  0.1, 0.1) # ROOT

    b2  = {}
    b2[(GOR, 0), 'h'] = 1.0
    b2[(GOR, 1), 'a'] = 1.0
    
    return DMV_Grammar({0:'h',1:'a'}, {'h':0,'a':1}, [ h_Aa, ha, a_Ah, ah, ra, _a_, a_S, a_A, a, rh, _h_, h_S, h_A, h ],b2,0,0,0)
def oa(s,t,LHS,loc_h):
    return outer(s,t,LHS,loc_h,testgrammar_a(),'h a'.split())
def ia(s,t,LHS,loc_h):
    return inner(s,t,LHS,loc_h,testgrammar_a(),'h a'.split())
def ca(s,t,LHS,loc_h):
    return c(s,t,LHS,loc_h,testgrammar_a(),'h a'.split())

def testgrammar_h():                            # Non, Adj
    _h_ = DMV_Rule((SEAL,0), STOP,    ( RGOL,0), 1.0, 1.0) # LSTOP
    h_S = DMV_Rule(( RGOL,0),(GOR,0),  STOP,    0.4, 0.3) # RSTOP
    h_A = DMV_Rule(( RGOL,0),(SEAL,0),( RGOL,0), 0.6, 0.7) # Lattach
    h   = DMV_Rule((GOR,0),(GOR,0),(SEAL,0), 1.0, 1.0) # Rattach
    rh  = DMV_Rule(   ROOT,   STOP,    (SEAL,0), 1.0, 1.0) # ROOT
    b2  = {}
    b2[(GOR, 0), 'h'] = 1.0
    
    return DMV_Grammar({0:'h'}, {'h':0}, [ rh, _h_, h_S, h_A, h ],b2,0,0,0)
    

def testreestimation_h():
    g = testgrammar_h()
    reestimate(g,['h h h'.split()])


def regression_tests():
    def test(wanted, got):
        if not wanted == got:
            print "Regression! Should be %s: %s" % (wanted, got)
            
    g_dup = testgrammar_h()
        
    test("0.120",
         "%.3f" % inner(0, 1, (SEAL,0), 0, g_dup, 'h h'.split(), {}))
    
    test("0.063",
         "%.3f" % inner(0, 1, (SEAL,0), 1, g_dup, 'h h'.split(), {}))
        
    test("0.0498",
         "%.4f" % inner(0, 2, (SEAL,0), 2, g_dup, 'h h h'.split(), {}))
    
    test("0.58" ,
         "%.2f" % outer(1,2,(1,0),2,testgrammar_h(),'h h h'.split(),{},{}))

    test("0.1089" ,
         "%.4f" % outer(0,0,(0,0),0,testgrammar_a(),'h a'.split(),{},{}))
    test("0.3600" ,
         "%.4f" % outer(0,1,(0,0),0,testgrammar_a(),'h a'.split(),{},{}))
    test("0.0000" ,
         "%.4f" % outer(0,2,(0,0),0,testgrammar_a(),'h a'.split(),{},{}))

    
if __name__ == "__main__":
    DEBUG.clear()
if __name__ == "__main__":
    regression_tests()
    testreestimation()

def testIO():
    g = testgrammar()
    inners = [(sent, inner_sent(g, sent, {})) for sent in testcorpus]
    return inners
